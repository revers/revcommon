/* 
 * File:   RevConfigReader.cpp
 * Author: Kamil
 * 
 * Created on 5 wrzesień 2012, 09:49
 *
 * This is modified version of http://twit88.com/blog/2008/02/27/unix-a-c-configuration-reader/
 *
 */


#include <cstdlib>
#include <fstream>
#include <iostream>

#include <rev/common/RevConfigReader.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevStringUtil.h>
#include <rev/common/RevAssert.h>

using namespace std;
using namespace rev;

ConfigReader& ConfigReader::getInstance() {
    static ConfigReader _instance;

    return _instance;
}

bool ConfigReader::load(const std::string& initFileName) {

    std::ifstream in(initFileName.c_str());

    if (!in) {
        alwaysAssertMsgNoExit(false, "WARNING: Cannot load '"
                << initFileName << "' configuration file!!");
        return false;
    }

    getInstance().clear();
    std::string fullLine, command;
    std::string leftSide, rightSide;
    std::string::size_type length;

    while (std::getline(in, fullLine)) {

        /* if the line contains a # then it is a comment
           if we find it anywhere other than the beginning, then we assume 
           there is a command on that line, and it we don't find it at all
           we assume there is a command on the line (we test for valid 
           command later) if neither is true, we continue with the next line
         */
        length = fullLine.find('#');
        if (length == std::string::npos) {
            command = fullLine;
        } else if (length > 0) {
            command = fullLine.substr(0, length);
        } else {
            continue;
        }

        // check the command and handle it
        length = command.find('=');
        if (length != std::string::npos) {
            leftSide = command.substr(0, length);
            rightSide = command.substr(length + 1, command.size() - length);
        } else {
            continue;
        }

        // add to the map of properties
        getInstance().insert(ConfigReader::value_type(StringUtil::trim(leftSide), StringUtil::trim(rightSide)));
    }

    // close the file
    in.close();

    return true;
}

int ConfigReader::getInt(const std::string& property, int defaultValue) {
    const_iterator key = find(property);

    return key == end() ? defaultValue : StringUtil::stringTo<int>(key->second);
}

unsigned int ConfigReader::getUint(const std::string& property, unsigned int defaultValue) {
    const_iterator key = find(property);

    return key == end() ? defaultValue : StringUtil::stringTo<unsigned int > (key->second);
}

bool ConfigReader::getBool(const std::string& property, bool defaultValue) {
    const_iterator key = find(property);

    return key == end() ? defaultValue : StringUtil::stringTo<bool>(key->second);
}

float ConfigReader::getFloat(const std::string& property, float defaultValue) {
    const_iterator key = find(property);

    return key == end() ? defaultValue : StringUtil::stringTo<float>(key->second);
}

double ConfigReader::getDouble(const std::string& property, double defaultValue) {
    const_iterator key = find(property);

    return key == end() ? defaultValue : StringUtil::stringTo<double>(key->second);
}

std::string ConfigReader::getString(const std::string& property,
        const char* defaultValue) {
    const_iterator key = find(property);
    return (key == end()) ? std::string(defaultValue) : (*key).second;
}
