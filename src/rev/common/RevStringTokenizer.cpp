/* 
 * File:   RevStringTokenizer.cpp
 * Author: Kamil
 * 
 * Created on 22 sierpień 2012, 16:38
 */

#include <rev/common/RevStringTokenizer.h>
#include "RevStringStringTokenizer.h"
#include "RevCharSetStringTokenizer.h"
#include "RevRegexStringTokenizer.h"

using namespace std;
using namespace rev;

const std::string StringTokenizer::WHITE_CHARACTERS = std::string(" \t\n\r\f");

StringTokenizerPtr
StringTokenizer::createTokenizerByCharSet(const std::string& str) {
    return createTokenizerByCharSet(str, StringTokenizer::WHITE_CHARACTERS);
}

StringTokenizerPtr
StringTokenizer::createTokenizerByCharSet(const std::string& str, const std::string& charSetDelimiter) {
    return StringTokenizerPtr(new CharSetStringTokenizer(str, charSetDelimiter));
}

StringTokenizerPtr
StringTokenizer::createTokenizerByString(const std::string& str, const std::string& stringDelimiter) {
    return StringTokenizerPtr(new StringStringTokenizer(str, stringDelimiter));
}

StringTokenizerPtr
StringTokenizer::createTokenizerByRegex(const std::string& str, const std::string& regexDelimiter,
        std::regex_constants::match_flag_type flags/* = std::regex_constants::match_default */) {
    return StringTokenizerPtr(new RegexStringTokenizer(str, regexDelimiter, flags));
}

StringTokenizerPtr
StringTokenizer::createTokenizerByCharSet() {
    return StringTokenizerPtr(new CharSetStringTokenizer());
}

StringTokenizerPtr
StringTokenizer::createTokenizerByString() {
    return StringTokenizerPtr(new StringStringTokenizer());
}

StringTokenizerPtr
StringTokenizer::createTokenizerByRegex(
        std::regex_constants::match_flag_type flags/* = std::regex_constants::match_default */) {
    return StringTokenizerPtr(new RegexStringTokenizer(flags));
}

StringVectorPtr StringTokenizer::split() {

    StringVectorPtr tokens(new std::vector<std::string > ());

    std::string token;
    while ((token = this->next()) != "") {
        tokens->push_back(token);
    }

    return tokens;
}

StringVectorPtr StringTokenizer::splitByCharSet(
        const std::string& str, const std::string& charSetDelimiter/* = WHITE_CHARACTERS*/) {
    CharSetStringTokenizer tokenizer(str, charSetDelimiter);
    return tokenizer.split();
}

StringVectorPtr StringTokenizer::splitByString(const std::string& str, const std::string& stringDelimiter) {
    StringStringTokenizer tokenizer(str, stringDelimiter);
    return tokenizer.split();
}

StringVectorPtr StringTokenizer::splitByRegex(const std::string& str, const std::string& regexDelimiter,
        std::regex_constants::match_flag_type flags/* = std::regex_constants::match_default*/) {
    RegexStringTokenizer tokenizer(str, regexDelimiter, flags);
    return tokenizer.split();
}