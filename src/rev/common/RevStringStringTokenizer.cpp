/* 
 * File:   RevStringStringTokenizer.cpp
 * 
 * Created on 22 sierpień 2012, 17:07
 */

#include <rev/common/RevStringTokenizer.h>
#include "RevStringStringTokenizer.h"

using namespace rev;

StringStringTokenizer::StringStringTokenizer()
: StringTokenizer("", " "), token("") {
}

StringStringTokenizer::StringStringTokenizer(const std::string& str,
        const std::string& delimiter)
: StringTokenizer(str, delimiter), token("") {
}

void StringStringTokenizer::set(const std::string& str, const std::string& delimiter) {
    this->buffer = str;
    this->delimiter = delimiter;
    this->currPos = 0;
}

void StringStringTokenizer::setString(const std::string& str) {
    this->buffer = str;
    this->currPos = 0;
}

void StringStringTokenizer::setDelimiter(const std::string& delimiter) {
    this->delimiter = delimiter;
    this->currPos = 0;
}

std::string StringStringTokenizer::next() {
    if (buffer.size() <= 0 || currPos >= buffer.length()) {
        return "";
    }

    int newPos = buffer.find(delimiter, currPos);
    if (newPos == std::string::npos) {
        int len = buffer.length() - currPos;
        token = buffer.substr(currPos, len);
        currPos += len;
        return token;
    }

    int len = newPos - currPos;
    token = buffer.substr(currPos, len);
    currPos += len + delimiter.length();
    return token;
}


