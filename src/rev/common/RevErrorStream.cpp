/* 
 * File:   RevErrorStream.cpp
 * Author: Kamil
 * 
 * Created on 10 sierpień 2012, 18:28
 */

#include <cstdio>

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>

using namespace rev;

rev::ErrorStream rev::errout;

bool rev::removeErrorFile() {
    bool errFileRemoved = (remove(REVCOMMON_ERROR_FILE) == 0);
    assertMsg(errFileRemoved, "Cannot remove '" << REVCOMMON_ERROR_FILE << "' file!");

    return errFileRemoved;
}
