/* 
 * File:   RevStringStringTokenizer.h
 *
 * The author of orignal version of this class is: 
 * Song Ho Ahn (song.ahn (at) gmail.com)
 * (http://www.songho.ca/)
 *
 * Created on 22 sierpień 2012, 17:07
 */

#ifndef REVSTRINGSTRINGTOKENIZER_H
#define	REVSTRINGSTRINGTOKENIZER_H

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevStringTokenizer.h>

namespace rev {

    class REVCOMMON_API StringStringTokenizer : public rev::StringTokenizer {
    private:
        std::string token; 
        int currPos = 0; 

        StringStringTokenizer(const StringStringTokenizer& orig);
    public:
        StringStringTokenizer(const std::string& str,
                const std::string& delimiter);

        StringStringTokenizer();

        /**
         * @Override
         */
        void setString(const std::string& str);

        /**
         * @Override
         */
        void setDelimiter(const std::string& delim);

        /**
         * @Override
         */
        void set(const std::string& str, const std::string& stringDelimiter);

        /**
         * @Override
         * returns "" if there are no more tokens.
         */
        std::string next();
    };
}

#endif	/* REVSTRINGSTRINGTOKENIZER_H */

