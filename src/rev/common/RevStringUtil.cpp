#include <rev/common/RevStringUtil.h>
#include <rev/common/RevAssert.h>
#include "ConvertUTF.h"

#include <stdexcept>
#include <exception>

using namespace rev;

std::wstring StringUtil::toWideString(const std::string& utf8string) {
    int widesize = utf8string.length();
    if (sizeof (wchar_t) == 2) {
        std::wstring resultstring;
        resultstring.resize(widesize + 1, L'\0');
        const UTF8* sourcestart = reinterpret_cast<const UTF8*> (utf8string.c_str());
        const UTF8* sourceend = sourcestart + widesize;
        UTF16* targetstart = reinterpret_cast<UTF16*> (&resultstring[0]);
        UTF16* targetend = targetstart + widesize;
        ConversionResult res = ConvertUTF8toUTF16(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
        if (res != conversionOK) {
            assertMsg(false, "Convertion Error!");
            throw std::logic_error("Convertion Error!");
        }
        *targetstart = 0;
        return resultstring;
    } else if (sizeof (wchar_t) == 4) {
        std::wstring resultstring;
        resultstring.resize(widesize + 1, L'\0');
        const UTF8* sourcestart = reinterpret_cast<const UTF8*> (utf8string.c_str());
        const UTF8* sourceend = sourcestart + widesize;
        UTF32* targetstart = reinterpret_cast<UTF32*> (&resultstring[0]);
        UTF32* targetend = targetstart + widesize;
        ConversionResult res = ConvertUTF8toUTF32(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
        if (res != conversionOK) {
            assertMsg(false, "Convertion Error!");
            throw std::logic_error("Convertion Error!");
        }
        *targetstart = 0;
        return resultstring;
    } else {
        assertMsg(false, "Convertion Error!");
        throw std::logic_error("Convertion Error!");
    }
    return L"";
}

std::string StringUtil::fromWideString(const std::wstring& widestring) {
    int widesize = widestring.length();

    if (sizeof (wchar_t) == 2) {
        int utf8size = 3 * widesize + 1;
        std::string resultstring;
        resultstring.resize(utf8size, '\0');
        const UTF16* sourcestart = reinterpret_cast<const UTF16*> (widestring.c_str());
        const UTF16* sourceend = sourcestart + widesize;
        UTF8* targetstart = reinterpret_cast<UTF8*> (&resultstring[0]);
        UTF8* targetend = targetstart + utf8size;
        ConversionResult res = ConvertUTF16toUTF8(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
        if (res != conversionOK) {
            assertMsg(false, "Convertion Error!");
            throw std::logic_error("Convertion Error!");
        }
        *targetstart = 0;
        return resultstring;
    } else if (sizeof (wchar_t) == 4) {
        int utf8size = 4 * widesize + 1;
        std::string resultstring;
        resultstring.resize(utf8size, '\0');
        const UTF32* sourcestart = reinterpret_cast<const UTF32*> (widestring.c_str());
        const UTF32* sourceend = sourcestart + widesize;
        UTF8* targetstart = reinterpret_cast<UTF8*> (&resultstring[0]);
        UTF8* targetend = targetstart + utf8size;
        ConversionResult res = ConvertUTF32toUTF8(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
        if (res != conversionOK) {
            assertMsg(false, "Convertion Error!");
            throw std::logic_error("Convertion Error!");
        }
        *targetstart = 0;
        return resultstring;
    } else {
        assertMsg(false, "Convertion Error!");
        throw std::logic_error("Convertion Error!");
    }
    return "";
}

std::string StringUtil::replaceStr(const std::string& src, const std::string& target, const std::string& replacement) {
    std::string result = src;

    if (result.length() == 0) {
        return result;
    }

    int idx = 0;

    for (;;) {
        idx = result.find(target, idx);
        if (idx == std::string::npos) break;

        result.replace(idx, target.length(), replacement);
        idx += replacement.length();
    }

    return result;
}

std::string StringUtil::format(const std::string& fmt, ...) {
    int size = 100;
    std::string str;
    va_list ap;
    while (1) {
        str.resize(size);
        va_start(ap, fmt);
        int n = vsnprintf((char *) str.c_str(), size, fmt.c_str(), ap);
        va_end(ap);
        if (n > -1 && n < size) {
            str.resize(n);
            return str;
        }
        if (n > -1)
            size = n + 1;
        else
            size *= 2;
    }
}