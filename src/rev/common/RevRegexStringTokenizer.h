/* 
 * File:   RevRegexStringTokenizer.h
 * Author: Kamil
 *
 * Created on 27 sierpień 2012, 13:28
 */

#ifndef REVREGEXSTRINGTOKENIZER_H
#define	REVREGEXSTRINGTOKENIZER_H

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevStringTokenizer.h>

#include <regex>

namespace rev {

    class REVCOMMON_API RegexStringTokenizer : public rev::StringTokenizer {
    private:
        std::regex_constants::match_flag_type flags;
        std::string token;
        int currPos = 0;

        RegexStringTokenizer(const RegexStringTokenizer& orig);
    public:
        RegexStringTokenizer(const std::string& str,
                const std::string& delimiter,
                std::regex_constants::match_flag_type flags_ = std::regex_constants::match_default);

        RegexStringTokenizer(std::regex_constants::match_flag_type flags_ = std::regex_constants::match_default);

        /**
         * @Override
         */
        void setString(const std::string& str);

        /**
         * @Override
         */
        void setDelimiter(const std::string& delim);

        /**
         * @Override
         */
        void set(const std::string& str, const std::string& stringDelimiter);

        /**
         * @Override
         * returns "" if there are no more tokens.
         */
        std::string next();
    };
}

#endif	/* REVREGEXSTRINGTOKENIZER_H */

