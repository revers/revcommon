/* 
 * File:   RevCommonUtil.cpp
 * Author: Revers
 * 
 * Created on 11 sierpień 2012, 09:06
 */

#include <rev/common/RevCommonUtil.h>
#include <string>
#include <cstring>
#include <ctime>
#include <stdio.h>

using namespace rev;

#define REV_DATE_FORMAT "%Y-%m-%d %X"
//                      "DD-MM-YYYY HH:mm:ss"

const std::string CommonUtil::getDate() {
    time_t now = time(0);
    struct tm tstruct;
    char buf[64];
    tstruct = *localtime(&now);
    // Visit http://www.cplusplus.com/reference/clibrary/ctime/strftime/
    // for more information about date/time format
    strftime(buf, sizeof (buf), REV_DATE_FORMAT, &tstruct);

    return buf;
}



