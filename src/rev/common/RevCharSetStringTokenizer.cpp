/* 
 * File:   RevCharSetStringTokenizer.cpp
 *
 * The author of orignal version of this class is: 
 * Song Ho Ahn (song.ahn (at) gmail.com)
 * (http://www.songho.ca/)
 * 
 * Created on 24 sierpień 2012, 16:57
 */

#include "RevCharSetStringTokenizer.h"
#include <rev/common/RevStringTokenizer.h>

using namespace rev;


///////////////////////////////////////////////////////////////////////////////
// constructor
///////////////////////////////////////////////////////////////////////////////

CharSetStringTokenizer::CharSetStringTokenizer()
: StringTokenizer("", StringTokenizer::WHITE_CHARACTERS), token("") {
    currPos = buffer.begin();
}

CharSetStringTokenizer::CharSetStringTokenizer(const std::string& str,
        const std::string& delimiter)
: StringTokenizer(str, delimiter), token("") {
    currPos = buffer.begin();
}

void CharSetStringTokenizer::set(const std::string& str, const std::string& delimiter) {
    this->buffer = str;
    this->delimiter = delimiter;
    this->currPos = buffer.begin();
}

void CharSetStringTokenizer::setString(const std::string& str) {
    this->buffer = str;
    this->currPos = buffer.begin();
}

void CharSetStringTokenizer::setDelimiter(const std::string& delimiter) {
    this->delimiter = delimiter;
    this->currPos = buffer.begin();
}



///////////////////////////////////////////////////////////////////////////////
// return the next token
// If cannot find a token anymore, return "".
///////////////////////////////////////////////////////////////////////////////

std::string CharSetStringTokenizer::next() {
    if (buffer.size() <= 0) {
        return ""; // skip if buffer is empty
    } 

    token.clear(); // reset token string

    this->skipDelimiter(); // skip leading delimiters

    // append each char to token string until it meets delimiter
    while (currPos != buffer.end() && !isDelimiter(*currPos)) {
        token += *currPos;
        ++currPos;
    }
    return token;
}



///////////////////////////////////////////////////////////////////////////////
// skip ang leading delimiters
///////////////////////////////////////////////////////////////////////////////

void CharSetStringTokenizer::skipDelimiter() {
    while (currPos != buffer.end() && isDelimiter(*currPos))
        ++currPos;
}



///////////////////////////////////////////////////////////////////////////////
// return true if the current character is delimiter
///////////////////////////////////////////////////////////////////////////////

bool CharSetStringTokenizer::isDelimiter(char c) {
    return (delimiter.find(c) != std::string::npos);
}



/////////////////////////////////////////////////////////////////////////////////
//// split the input string into multiple tokens
//// This function scans tokens from the current cursor position.
/////////////////////////////////////////////////////////////////////////////////
//
//std::vector<std::string> CharSetStringTokenizer::split() {
//    std::vector<std::string> tokens;
//    std::string token;
//    while ((token = this->next()) != "") {
//        tokens.push_back(token);
//    }
//
//    return tokens;
//}
