/* 
 * File:   RevXMLElement.cpp
 * Author: Kamil
 * 
 * Created on 7 sierpień 2012, 15:20
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/xml/RevXMLElement.h>
#include <fstream>
#include <iostream>
#include <cstring>

#include <pugixml.hpp>

using namespace rev;
using namespace std;

#ifndef NDBUG
#define SHOW_ERROR(a) assertMsg(false, "XML ERROR: " << a)
#else
#define SHOW_ERROR(a) REV_ERROR_MSG("XML ERROR: " << a)
#endif

#define SHOW_WARNING(a) REV_ERROR_MSG("XML WARNING: " << a)

XMLElementMetadata::~XMLElementMetadata() {
    if (children) {
        for (auto& elemPtr : *(children)) {
            delete elemPtr.second;
        }
        delete children;
    }

    if (attributes) {
        for (auto& attrPtr : *attributes) {
            delete attrPtr.second;
        }

        delete attributes;
    }

    if (value) {
        delete value;
    }
}

void XMLElementMetadata::print(std::ostream& out, const std::string& indent) {
    out << indent << "Element: '" << name << "'" << endl;
    if (value != NULL) {
        out << indent << "  Value: '" << value->getValueString() << "'" << endl;
    } else {
        out << indent << "  Value: <EMPTY>" << endl;
    }

    if (attributes != NULL) {
        out << indent << "  Attributes: " << endl;
        for (auto& pair : *attributes) {
            out << indent << "    ['" << pair.first << "' = '" << pair.second->getValueString() << "']" << endl;
        }
    } else {
        out << indent << "  Attributes: <EMPTY>" << endl;
    }

    if (children != NULL) {
        out << indent << "  Children:" << endl;
        for (auto& pair : *children) {
            out << indent << "    ['" << pair.first << "' = " << endl;
            pair.second->getMetadata().print(out, indent + "      ");
            out << indent << "    ]" << endl;
        }
    } else {
        out << indent << "  Children: <EMPTY>" << endl;
    }
}

inline bool strEquals(const char* a, const char* b) {
    return strcmp(a, b) == 0;
}

inline bool strNotEquals(const char* a, const char* b) {
    return strcmp(a, b) != 0;
}

inline bool strEmpty(const char* a) {
    return a == NULL || strcmp(a, "") == 0;
}

inline bool strNotEmpty(const char* a) {
    return a != NULL && strcmp(a, "") != 0;
}

#define NODE_NAME(n) "Node's name: '" << n->name() << "'; "
#define NODE_VALUE "Node's value: '" << node->value() << "'; "
#define NODE_ATTR_NAME "Node attribute's name: '" << xmlAttr.name() << "'; " 
#define NODE_ATTR_VALUE "Node attribute's value: '" << xmlAttr.value() << "'; " 

#define ELEMENT_NAME "Element's name: '" << element->getXMLElementName() << "'; "
#define ELEMENT_ATTR_NAME "Element attribute's name: '" << attrName << "'; " 

bool XMLElement::load(XMLElement* element, pugi::xml_node* node) {
    // Just in case. Works only once:
    element->initXMLElement();

    XMLElementMetadata& m = element->getMetadata();

    if (strNotEquals(element->getXMLElementName(), node->name())) {
        SHOW_ERROR("Node's name not equals Element's name!\n\t"
                << ELEMENT_NAME << NODE_NAME(node));
        return false;
    }

    if (strNotEmpty(node->value()) && m.value == NULL) {
        SHOW_ERROR("Node's value not mapped!\n\t"
                << ELEMENT_NAME << NODE_NAME(node) << NODE_VALUE);
        return false;
    }

    if (element->hasMappedValue()) {
        element->setXMLElementValue(node->value());
    }

    for (pugi::xml_attribute& xmlAttr : node->attributes()) {

        if (!element->setXMLElementAttribute(xmlAttr.name(), xmlAttr.value())) {
            SHOW_ERROR("Node's attribute not mapped!\n\t"
                    << NODE_NAME(node) << NODE_ATTR_NAME << NODE_ATTR_VALUE);
            return false;
        }
    }

    for (pugi::xml_node& child : node->children()) {
        XMLElement* metaElement = element->getXMLChildMetaElement(child.name());

        if (metaElement == NULL) {
            SHOW_ERROR("Child node not mapped!\n\tParent "
                    << ELEMENT_NAME << NODE_NAME((&child)));
            return false;
        }

        rev::XMLFactoryRegistry& registry = rev::XMLFactoryRegistry::getInstance();
        std::string factoryName = child.name();
        factoryName += "@";
        factoryName += metaElement->getClassName();
        XMLElementFactory factory = registry.getFactory(factoryName.c_str());

        if (factory == NULL) {
            SHOW_ERROR("There is no registered factory for element '"
                    << factoryName << "'!");
            return false;
        }

        XMLElement* elem = factory();

        load(elem, &child);

        assert(metaElement->siblingElementsPtr);
        metaElement->siblingElementsPtr->addElement(elem);
    }

    return true;
}

const char* XMLElement::getClassName() {
    static const char* name = "XMLElement";
    return name;
}

bool XMLElement::loadFromStream(std::istream& in) {
    pugi::xml_document doc;
    pugi::xml_parse_result result = doc.load(in);

    if (!result) {
        SHOW_ERROR("Parsing error: " << result.description());
        return false;
    }

    pugi::xml_node root = doc.first_child();

    return load(this, &root);
}

bool XMLElement::loadFromFile(const char* filename) {

    ifstream in(filename);
    if (!in) {
        SHOW_ERROR("FILE NOT FOUND '" << filename << "'");
        return false;
    }

    return loadFromStream(in);
}

bool XMLElement::writeToFile(const char* filename) {

    ofstream out(filename);
    if (out.bad()) {
        assertMsg(false, "Failed to write '" << filename << "' file!");
        return false;
    }

    printToStream(out);

    if (out.bad()) {
        assertMsg(false, "Failed to write '" << filename << "' file!");
        return false;
    }
    out.close();

    return true;
}

void XMLElement::printToStream(XMLElement* element, std::ostream& out,
        const std::string& indent) {

    out << indent << "<" << element->getXMLElementName();
    XMLAttributes* attributes = element->meta.attributes;

    if (attributes) {
        for (auto& attrPair : *attributes) {
            out << " " << attrPair.first << "=\""
                    << attrPair.second->getValueString() << "\"";
        }
    }


    string subIndent = indent + REV_SUB_INDENT;

    XMLElements* children = element->meta.children;

    bool hasChildren = false;
    if (children) {
        for (auto& elementPair : *children) {
            XMLElement* metaElement = elementPair.second;

            IXMLElementIterator* it = metaElement->siblingIterator();
            if (!it) {
                continue;
            }

            while (it->hasNextElement()) {
                XMLElement* sibling = it->nextElement();
                if (!hasChildren) {
                    out << ">" << endl;
                    hasChildren = true;
                }
                printToStream(sibling, out, subIndent);
            }

            delete it;
        }
    }


    bool hasValue = false;
    if (element->hasMappedValue()) {
		if (!hasChildren) {
			out << ">" << endl;
			hasChildren = true;
		}
        out << subIndent << element->getXMLElementValue() << endl;
        hasValue = true;
    }


    if (!hasChildren && !hasValue) {
        out << "/>" << endl;
    } else {
        out << indent << "</" << element->getXMLElementName() << ">" << endl;
    }
}

void XMLElement::printToStream(std::ostream& out, bool withHeader /* = true */) {
    if (withHeader) {
        out << "<?xml version=\"1.0\"?>" << endl;
    }
    printToStream(this, out, "");
}


