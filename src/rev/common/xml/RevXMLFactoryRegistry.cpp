/* 
 * File:   RevXMLFactoryRegistry.cpp
 * Author: Kamil
 * 
 * Created on 7 sierpień 2012, 17:31
 */

#include <rev/common/xml/RevXMLFactoryRegistry.h>

using namespace rev;

XMLFactoryRegistry& XMLFactoryRegistry::getInstance() {
    static XMLFactoryRegistry registry;

    return registry;
}


