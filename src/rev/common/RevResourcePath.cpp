/* 
 * File:   RevResourcePath.cpp
 * Author: Revers
 * 
 * Created on 24 marzec 2013, 16:07
 */

#include <algorithm>
#include <sys/types.h>
#include <sys/stat.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevStringUtil.h>
#include <rev/common/RevConfigReader.h>
#include <rev/common/RevResourcePath.h>
//#include "RevResourcePath.h"

using namespace rev;

bool rev::ResourcePath::inited;
std::string rev::ResourcePath::programPath;
std::string rev::ResourcePath::preferredPath;
std::string rev::ResourcePath::activePath;

void ResourcePath::init(int argc, char** argv) {
	inited = true;
	programPath = argv[0];
	std::replace(programPath.begin(), programPath.end(), '\\', '/');
	unsigned found = programPath.find_last_of("/");
	programPath = programPath.substr(0, found + 1);

	preferredPath = ConfigReader::ref().operator [](REV_RESOURCES_PATH_KEY);
	if (preferredPath == "") {
		activePath = programPath;
		REV_DEBUG_MSG("programPath = " << programPath << "\npreferredPath [PREF_NOT_FOUND:"
				REV_RESOURCES_PATH_KEY "] = " << preferredPath << "\nactivePath = " << activePath);
		return;
	}

	std::replace(preferredPath.begin(), preferredPath.end(), '\\', '/');
	if (preferredPath[preferredPath.size() - 1] != '/') {
		preferredPath += '/';
	}

	struct stat st;

	std::string statString = preferredPath.substr(0, preferredPath.size() - 1);
	if (stat(statString.c_str(), &st) == 0) {
		if (st.st_mode & S_IFDIR != 0) {
			activePath = preferredPath;
		} else {
			activePath = programPath;
			REV_DEBUG_MSG("Directory '" << preferredPath << "' doesn't exist!");
		}
	} else {
		REV_WARN_MSG("stat failed for string '" << statString << "'");
		activePath = programPath;
	}

	REV_DEBUG_MSG("programPath = " << programPath << "\npreferredPath = "
			<< preferredPath << "\nactivePath = " << activePath);
}

std::string ResourcePath::get(const std::string& path, bool preferred) {
	revAssert(inited);
	if (!ResourcePath::isRelative(path)) {
		REV_WARN_MSG("Getting not relative path: " << path);
		return path;
	}
	std::string fileStr(path);
	std::replace(fileStr.begin(), fileStr.end(), '\\', '/');

	if (preferred) {
		return activePath + fileStr;
	}
	return programPath + fileStr;
}

std::string ResourcePath::makeRelative(const std::string& path) {
	revAssert(inited);
	if (isRelative(path)) {
		return path;
	}

	std::string fileStr(path);
	std::replace(fileStr.begin(), fileStr.end(), '\\', '/');

#ifdef REVCOMMON_WINDOWS
	if (!StringUtil::startsWith(StringUtil::toLower(fileStr),
			StringUtil::toLower(programPath))) {
		REV_WARN_MSG("Path: '" << fileStr << "' is not a sub-path of program's directory: '"
				<< programPath << "'!!");
		return fileStr;
	}
#else
	if (!StringUtil::startsWith(fileStr, programPath)) {
		REV_WARN_MSG("Path: '" << fileStr << "' is not a sub-path of program's directory: '"
				<< programPath << "'!!");
		return fileStr;
	}
#endif

	return fileStr.substr(programPath.size(), fileStr.size() - programPath.size());
}

bool ResourcePath::isRelative(const std::string& path) {
	if (path.empty()) {
		return true;
	}

#ifdef REVCOMMON_WINDOWS
	if (path.find(':') != std::string::npos) {
		return false;
	}
#else
	if (path[0] == '/' || path[0] == '\\') {
		return false;
	}
#endif

	return true;
}
