/* 
 * File:   RevRegexStringTokenizer.cpp
 * Author: Kamil
 * 
 * Created on 27 sierpień 2012, 13:28
 */

#include <rev/common/RevStringTokenizer.h>
#include "RevRegexStringTokenizer.h"

using namespace rev;

RegexStringTokenizer::RegexStringTokenizer(
        std::regex_constants::match_flag_type flags_/* = std::regex_constants::match_default*/)
: StringTokenizer("", " "), flags(flags_), token("") {
}

RegexStringTokenizer::RegexStringTokenizer(
        const std::string& str,
        const std::string& delimiter,
        std::regex_constants::match_flag_type flags_/* = std::regex_constants::match_default*/)
: StringTokenizer(str, delimiter), flags(flags_), token("") {
}

void RegexStringTokenizer::set(const std::string& str, const std::string& delimiter) {
    this->buffer = str;
    this->delimiter = delimiter;
    this->currPos = 0;
}

void RegexStringTokenizer::setString(const std::string& str) {
    this->buffer = str;
    this->currPos = 0;
}

void RegexStringTokenizer::setDelimiter(const std::string& delimiter) {
    this->delimiter = delimiter;
    this->currPos = 0;
}

std::string RegexStringTokenizer::next() {
    if (buffer.size() <= 0 || currPos >= buffer.length()) {
        return "";
    }

    int newPos = buffer.find(delimiter, currPos);
    if (newPos == std::string::npos) {
        int len = buffer.length() - currPos;
        token = buffer.substr(currPos, len);
        currPos += len;
        return token;
    }

    int len = newPos - currPos;
    token = buffer.substr(currPos, len);
    currPos += len + delimiter.length();
    return token;
}