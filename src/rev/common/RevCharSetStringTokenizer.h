/* 
 * File:   RevCharSetStringTokenizer.h
 * 
 * The author of orignal version of this class is: 
 * Song Ho Ahn (song.ahn (at) gmail.com)
 * (http://www.songho.ca/)
 *
 * Created on 24 sierpień 2012, 16:57
 */

#ifndef REVCHARSETSTRINGTOKENIZER_H
#define	REVCHARSETSTRINGTOKENIZER_H

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevStringTokenizer.h>

namespace rev {

    class REVCOMMON_API CharSetStringTokenizer : public rev::StringTokenizer {
    private:
        void skipDelimiter(); // ignore leading delimiters
        bool isDelimiter(char c); // check if the current char is delimiter

        std::string token; // output string
        std::string::const_iterator currPos; // string iterator pointing the current position

        CharSetStringTokenizer(const CharSetStringTokenizer& orig);
    public:
        CharSetStringTokenizer(const std::string& str,
                const std::string& delimiter);

        CharSetStringTokenizer();

        /**
         * @Override
         */
        void setString(const std::string& str);

        /**
         * @Override
         */
        void setDelimiter(const std::string& delim);

        /**
         * @Override
         */
        void set(const std::string& str, const std::string& stringDelimiter);

        /**
         * @Override
         * returns "" if there are no more tokens.
         */
        std::string next();
    };
}

#endif	/* REVCHARSETSTRINGTOKENIZER_H */

