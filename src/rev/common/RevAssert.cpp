#include <rev/common/RevAssert.h>
#include <rev/common/RevCommonUtil.h>

#include <cstdio>
#include <cstdlib>

void rev::errorHandler(
        const char* expr,
        const char* function,
        const char* file,
        long line) {

    REV_ERROR_MSG("ASSERTION ERROR: "
            << expr << "; " << function << "\n\t"
            << file << " @ line " << line);
    fflush(stdout);

    __asm("int $3");
}

void rev::errorHandlerNoExit(
        const char* expr,
        const char* function,
        const char* file,
        long line) {

    REV_ERROR_MSG("ASSERTION ERROR: " << expr << "; "
            << function << "\n\t"
            << file << " @ line " << line);
    fflush(stdout);
}

void rev::errorHandlerNoRev(
        const char* expr,
        const char* function,
        const char* file,
        long line) {

    printf("ERROR: %s; %s\n\t%s @ line %d\n",
            expr,
            function,
            file, line);
    fflush(stdout);

    __asm("int $3");
}

void rev::errorHandlerNoRevNoExit(
        const char* expr,
        const char* function,
        const char* file,
        long line) {

    printf("ERROR: %s; %s\n\t%s @ line %d\n",
            expr,
            function,
            file, line);
    fflush(stdout);
}

void rev::errorHandlerMsg(
        const char* expr,
        const char* function,
        const char* file,
        long line,
        std::ostream& out) {

    out << "\nASSERTION ERROR: "
            << expr << "; " << function << "\n\t"
            << file << " @ line " << line << std::endl;
    fflush(stdout);

    __asm("int $3");
}

void rev::errorHandlerMsgNoExit(
        const char* expr,
        const char* function,
        const char* file,
        long line,
        std::ostream& out) {

    out << "\nASSERTION ERROR: " 
            << expr << "; " << function << "\n\t"
            << file << " @ line " << line << std::endl;

    fflush(stdout);
}
