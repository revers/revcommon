#include <string>
#include <rev/common/RevStringTokenizer.h>

#include <gtest/gtest.h>

using namespace std;
using namespace rev;

TEST(StringTokenizer, CharVsStringTokenizer) {
    string txt = "This is a testng string.Try tom od ify ity ourself.";

    StringTokenizerPtr charStrTok =
            StringTokenizer::createTokenizerByCharSet(txt);
    string charToken;

    StringTokenizerPtr stringStrTok =
            StringTokenizer::createTokenizerByString(txt, " ");
    string stringToken;

    while ((charToken = charStrTok->next()) != "") {
        stringToken = stringStrTok->next();
        ASSERT_EQ(charToken, stringToken);
    }
}