/* 
 * File:   RevAssert.h
 * Author: Revers
 *
 * Created on 28 lipiec 2012, 08:11
 */

#ifndef REVASSERT_H
#define	REVASSERT_H

#include <iostream>
#include <sstream>

#include "RevCommonConfig.h"
#include "RevAssertMinimal.h"
#include "RevCommonUtil.h"
#include "RevErrorStream.h"

namespace rev {
	REVCOMMON_API void errorHandlerMsg(const char* expr, const char* function,
			const char* file, long line, std::ostream& out);

	REVCOMMON_API void errorHandlerMsgNoExit(const char* expr, const char* function,
			const char* file, long line, std::ostream& out);

	namespace prv {
		template<typename T>
		std::string val(const char* name, const T& value) {
			std::ostringstream out;
			out << name << " = " << value;
			return out.str();
		}
	}

	/**
	 * Arguments to String helper.
	 */
	inline std::string a2s() {
		return "";
	}

	/**
	 * Arguments to String.
	 */
	template<typename T, typename ... Types>
	std::string a2s(const T& firstArg, const Types&... args) {
		std::ostringstream out;
		out << firstArg << ", " << a2s(args...);
		return out.str();
	}
}

#ifdef NDEBUG
/*
 * If not debugging, assert does nothing.
 */
#define assertMsg(expr, msg)	((void)0)
#define assertMsgNoExit(expr, msg) ((void)0)

#else /* debugging enabled */

#define assertMsg(expr, msg) ((expr) ? ((void)0) : \
        rev::errorHandlerMsg(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE, \
        REV_ERROR_MSG(msg)))

#define assertMsgNoExit(expr, msg)	 ((expr) ? ((void)0) : \
        rev::errorHandlerMsgNoExit(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE, \
        REV_ERROR_MSG(msg)));

#endif

/**
 * When assertion fail only message is printed out.
 */
#define alwaysAssertMsgNoExit(expr, msg) ((expr) ? ((void)0) : \
        rev::errorHandlerMsgNoExit(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE, \
        REV_ERROR_MSG(msg)))

/**
 * When assertion fail, message is printed out 
 * and program exits with -1 error code.
 */
#define alwaysAssertMsg(expr, msg) ((expr) ? ((void)0) : \
        rev::errorHandlerMsg(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE, \
        REV_ERROR_MSG(msg)))

#define assertTODO(x) alwaysAssertMsg(false, #x)
#define assertTODOMsg(x) alwaysAssertMsg(false, x)

#define revAssertMsg(expr, msg) assertMsg(expr, msg)
#define revAssertMsgNoExit(expr, msg) assertMsgNoExit(expr, msg)
#define revAlwaysAssertMsgNoExit(expr, msg) alwaysAssertMsgNoExit(expr, msg)
#define revAlwaysAssertMsg(expr, msg) alwaysAssertMsg(expr, msg)
#define revAssertTODO(x) assertTODO(x)
#define revAssertTODOMsg(x) assertTODOMsg(x)

#define R(X) rev::prv::val(#X, (X))

#endif	/* REVASSERT_H */

