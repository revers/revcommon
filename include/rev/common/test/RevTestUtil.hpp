/*
 * RevTestUtil.hpp
 *
 *  Created on: 19-09-2012
 *      Author: Kamil
 */

#ifndef REVTESTUTIL_HPP_
#define REVTESTUTIL_HPP_

#include <gtest/gtest.h>

#include <rev/common/test/RevColorUtil.h>
#include <rev/common/RevCommonUtil.h>
#include <rev/common/RevProfiler.h>
#include <rev/common/RevStringUtil.h>

#define DEFAULT_BG_COLOR rev::bg_white
#define DEFAULT_FG_COLOR rev::fg_black
#define DEFAULT_COLOR DEFAULT_FG_COLOR
#define TITLE_COLOR rev::fg_blue
#define EMPHASIS_COLOR rev::fg_red
#define TIME_COLOR rev::fg_lo_green
#define FADED_COLOR rev::fg_gray
#define WIDTH_COLOR rev::fg_lo_red
#define PARAMETERS_COLOR rev::fg_lo_red

#define REVP(COLOR, VAL) COLOR << VAL << DEFAULT_COLOR

namespace rev {

	class TestUtil {
	public:
		static void printSize(const char* name, int size) {

			std::cout << FADED_COLOR;

			std::cout << "Size of " << name << " \t= " << CommonUtil::getSizeMB(size)
					<< std::endl;

			std::cout << DEFAULT_FG_COLOR;
		}

		static void printProfilingInfo(const char* name, const Profiler& p) {

			std::cout << TIME_COLOR;

			std::cout << "Time for '" << name << "':\t" << p.getTotalTimeMicro() << " micro seconds. ("
			<< StringUtil::format("%.6f", p.getTotalTimeSec()) << " seconds.)" << std::endl;

			std::cout << DEFAULT_FG_COLOR;
		}

		static void printHeader() {
			std::cout << DEFAULT_BG_COLOR;
			const ::testing::TestInfo * const test_info =
			::testing::UnitTest::GetInstance()->current_test_info();
			std::cout << "BEGINNING TEST: -------------------------------------- "
			<< EMPHASIS_COLOR << test_info->test_case_name() << "." << test_info->name()
			<< DEFAULT_FG_COLOR << " ---------------------------------------" << std::endl;
		}

		static void printTitle(const std::string& title, const std::string& parameters) {
			std::cout << TITLE_COLOR << "\n\nTESTING '"
			<< EMPHASIS_COLOR << title << TITLE_COLOR
			<< "' with parameters\n"
			<< PARAMETERS_COLOR << parameters << "\n\n";
			std::cout << DEFAULT_FG_COLOR;
		}
	};
} // end namespace rev

#endif /* REVTESTUTIL_HPP_ */
