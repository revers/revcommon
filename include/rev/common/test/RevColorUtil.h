/* 
 * File:   RevColorUtil.h
 * Author: Revers
 *
 * Created on 6 wrzesień 2012, 22:00
 */

#ifndef REVCOLORUTIL_H
#define	REVCOLORUTIL_H

#include <ostream>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevConfigReader.h>

namespace rev {

    inline std::ostream& fg_black(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_BLACK;
        }

        return out;
    }

    inline std::ostream& fg_gray(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_GRAY;
        }

        return out;
    }

    inline std::ostream& fg_white(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_WHITE;
        }

        return out;
    }

    inline std::ostream& fg_red(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_RED;
        }

        return out;
    }

    inline std::ostream& fg_green(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_GREEN;
        }

        return out;
    }

    inline std::ostream& fg_blue(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_BLUE;
        }

        return out;
    }

    inline std::ostream& fg_cyan(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_CYAN;
        }

        return out;
    }

    inline std::ostream& fg_magenta(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_MAGENTA;
        }

        return out;
    }

    inline std::ostream& fg_yellow(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_YELLOW;
        }

        return out;
    }

    inline std::ostream& bg_black(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_BLACK;
        }

        return out;
    }

    inline std::ostream& bg_gray(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_GRAY;
        }

        return out;
    }

    inline std::ostream& bg_white(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_WHITE;
        }

        return out;
    }

    inline std::ostream& bg_red(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_RED;
        }

        return out;
    }

    inline std::ostream& bg_green(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_GREEN;
        }

        return out;
    }

    inline std::ostream& bg_blue(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_BLUE;
        }

        return out;
    }

    inline std::ostream& bg_cyan(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_CYAN;
        }

        return out;
    }

    inline std::ostream& bg_magenta(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_MAGENTA;
        }

        return out;
    }

    inline std::ostream& bg_yellow(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_YELLOW;
        }

        return out;
    }

    inline std::ostream& fg_lo_white(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_WHITE;
        }

        return out;
    }

    inline std::ostream& fg_lo_red(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_RED;
        }

        return out;
    }

    inline std::ostream& fg_lo_green(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_GREEN;
        }

        return out;
    }

    inline std::ostream& fg_lo_blue(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_BLUE;
        }

        return out;
    }

    inline std::ostream& fg_lo_cyan(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_CYAN;
        }

        return out;
    }

    inline std::ostream& fg_lo_magenta(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_MAGENTA;
        }

        return out;
    }

    inline std::ostream& fg_lo_yellow(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_FG_LO_YELLOW;
        }

        return out;
    }

    inline std::ostream& bg_lo_white(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_WHITE;
        }

        return out;
    }

    inline std::ostream& bg_lo_red(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_RED;
        }

        return out;
    }

    inline std::ostream& bg_lo_green(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_GREEN;
        }

        return out;
    }

    inline std::ostream& bg_lo_blue(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_BLUE;
        }

        return out;
    }

    inline std::ostream& bg_lo_cyan(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_CYAN;
        }

        return out;
    }

    inline std::ostream& bg_lo_magenta(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_MAGENTA;
        }

        return out;
    }

    inline std::ostream& bg_lo_yellow(std::ostream& out) {
        if (ConfigReader::getInstance().getBool("output.coloring", true)) {
            out << REV_BG_LO_YELLOW;
        }

        return out;
    }
}

#endif	/* REVCOLORUTIL_H */

