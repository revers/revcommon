/*
 * RevUnref.h
 *
 *  Created on: 02-03-2013
 *      Author: Revers
 */

#ifndef REVUNREF_H_
#define REVUNREF_H_

namespace rev {

    template<class T> struct unref {
        typedef T type;
    };

    template<class T> struct unref<T&> {
        typedef T type;
    };

    template<class T> struct unref<T*> {
        typedef T type;
    };
}

#define THIS_TYPE rev::unref<decltype(this)>::type

#endif /* REVUNREF_H_ */
