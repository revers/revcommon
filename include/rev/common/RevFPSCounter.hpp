/* 
 * File:   FPSCounter.h
 * Author: Revers
 *
 * Created on 18 grudzień 2011, 01:05
 */

#ifndef REVFPSCOUNTER_H
#define	REVFPSCOUNTER_H

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevTimer.h>

namespace rev {

    class FPSCounter {
        rev::Timer timer;
        double t1;
        double t2;
        int frameCount;
        double framesPerSec;
        double totalTime;
        double frameTime;
        double notificationFrequency;

    public:
        FPSCounter() {
            notificationFrequency = 1.0 / 60.0;
            frameCount = 0;
            totalTime = 0.0;
            timer.start();
        }

        FPSCounter(double notificationFrequency_) :
                notificationFrequency(notificationFrequency_) {
            frameCount = 0;
            totalTime = 0.0;
            timer.start();
        }

        virtual ~FPSCounter() {
        }

        void frameBegin() {
            t1 = timer.getTimeInSec();
            frameCount++;
        }

        // notficationFrequency = how often frameEnd() function will return "true":
        bool frameEnd() {
            t2 = timer.getTimeInSec();

            frameTime = t2 - t1;
            totalTime += frameTime;

            if (totalTime >= notificationFrequency) {
                framesPerSec = (double) frameCount / totalTime;
                frameCount = 0;
                totalTime = 0.0;
                return true;
            }

            return false;
        }

        double getFramesPerSec() const {
            return framesPerSec;
        }

        // in seconds:
        double getFrameTime() const {
            return frameTime;
        }

        // in seconds:
        double getNotificationFrequency() const {
            return notificationFrequency;
        }

        // in seconds:
        void setNotificationFrequency(double notificationFrequency) {
            this->notificationFrequency = notificationFrequency;
        }
    };
}
#endif
