/*
 * RevFixedSizedVector.hpp
 *
 *  Created on: 06-12-2012
 *      Author: Revers
 */

#ifndef REVFIXEDSIZEDVECTOR_HPP_
#define REVFIXEDSIZEDVECTOR_HPP_

#include "RevArrayIterator.hpp"
#include <rev/common/RevAssert.h>

namespace rev {

    template<typename T, int SIZE>
    class FixedSizedVector {
        T arr[SIZE];
        int currentSize;
        public:
        FixedSizedVector() :
                currentSize(0) {
        }

        T& operator[](int index) {
            assert(index < currentSize);
            return arr[index];
        }

        const T& operator[](int index) const {
            assert(index < currentSize);
            return arr[index];
        }

        void push_back(const T& val) {
            assert(currentSize < SIZE);
            arr[currentSize++] = val;
        }

        ArrayIterator<T> begin() {
            return ArrayIterator<T>(arr);
        }

        ArrayIterator<T> end() {
            return ArrayIterator<T>(arr + currentSize);
        }
    };
}

#endif /* REVFIXEDSIZEDVECTOR_HPP_ */
