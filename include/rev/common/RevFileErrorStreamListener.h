/* 
 * File:   RevFileErrorStreamListener.h
 * Author: Kamil
 *
 * Created on 14 sierpień 2012, 14:25
 */

#ifndef REVFILEERRORSTREAMLISTENER_H
#define	REVFILEERRORSTREAMLISTENER_H



#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevIErrorStreamListener.h>
#include <rev/common/RevAssertMinimal.h>
#include <fstream>
#include <iostream>
#include <cstdio>

namespace rev {

    class REVCOMMON_API FileErrorStreamListener : public rev::IErrorStreamListener {
        std::ofstream fileOut;
    public:

        FileErrorStreamListener(const char* filename) {
            // if file exists delete it:
            if (FILE * file = fopen(filename, "r")) {
                fclose(file);
                bool errFileRemoved = (remove(filename) == 0);
                alwaysAssertNoRevNoExit(errFileRemoved);
            }

            fileOut.open(filename, std::ios::out | std::ios::app);

            alwaysAssertNoRev(fileOut.good());
        }

        /**
         * @Override
         */
        virtual void print(const char* line) {
            fileOut << line << std::endl;
        }
    };
}

#endif	/* REVFILEERRORSTREAMLISTENER_H */

