/* 
 * File:   RevAssertMinimal.h
 * Author: Kamil
 *
 * Created on 10 sierpień 2012, 18:58
 */

#ifndef REVASSERTMINIMAL_H
#define	REVASSERTMINIMAL_H

#define REV_ASSERT_AVAILABLE

#ifdef assert
#undef assert
#endif

namespace rev {
    REVCOMMON_API void errorHandler(
            const char* expr,
            const char* function,
            const char* file,
            long line);

    REVCOMMON_API void errorHandlerNoExit(
            const char* expr,
            const char* function,
            const char* file,
            long line);
    
    REVCOMMON_API void errorHandlerNoRev(
            const char* expr,
            const char* function,
            const char* file,
            long line);

    REVCOMMON_API void errorHandlerNoRevNoExit(
            const char* expr,
            const char* function,
            const char* file,
            long line);
}

#define REVCOMMON_DEBUG_FILE __FILE__
#define REVCOMMON_DEBUG_LINE __LINE__
#define REVCOMMON_DEBUG_FUNCTION __PRETTY_FUNCTION__

#ifdef NDEBUG
/**
 * If not debugging, assert does nothing.
 */
#define assert(x)	((void)0)
#define revAssert(x)   ((void)0)
#define assertNoRev(x)  ((void)0)

#define assertNoExit(x) ((void)0)
#define assertNoRevNoExit(x) ((void)0)

#else /* debugging enabled */

#define assert(expr) ((expr) ? ((void)0) : \
        rev::errorHandler(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE))

#define revAssert(expr) ((expr) ? ((void)0) : \
        rev::errorHandler(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE))

/**
 *  assert without using rev::errout (just plain printf):
 */
#define assertNoRev(expr) ((expr) ? ((void)0) : \
        rev::errorHandlerNoRev(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

/**
 *  When expr equals false, only error message is printed out (without exit):
 */		
#define assertNoExit(expr) ((expr) ? ((void)0) : \
        rev::errorHandlerNoExit(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

/**
 *  assertNoExit without using rev::errout (just plain printf):
 */
#define assertNoRevNoExit(expr) ((expr) ? ((void)0) : \
        rev::errorHandlerNoRevNoExit(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

#endif

/**
 * When assertion fail only message is printed out.
 */
#define alwaysAssertNoExit(expr) ((expr) ? ((void)0) : \
        rev::errorHandlerNoExit(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

/**
 * alwaysAssertNoExit without using rev::errout (just plain printf):
 */
#define alwaysAssertNoRevNoExit(expr) ((expr) ? ((void)0) : \
        rev::errorHandlerNoRevNoExit(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

/**
 * When assertion fail, message is printed out 
 * and program exits with -1 error code.
 */
#define alwaysAssert(expr) ((expr) ? ((void)0) : \
        rev::errorHandler(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

/**
 * alwaysAssert without using rev::errout (just plain printf):
 */
#define alwaysAssertNoRev(expr) ((expr) ? ((void)0) : \
        rev::errorHandlerNoRev(#expr, \
        REVCOMMON_DEBUG_FUNCTION, \
        REVCOMMON_DEBUG_FILE, \
        REVCOMMON_DEBUG_LINE));

#define revAssertNoExit(expr) assertNoExit(expr)
#define revAlwaysAssertNoExit(expr) alwaysAssertNoExit(expr)

#endif	/* REVASSERTMINIMAL_H */

#ifdef assert
#undef assert
#define assert(x) revAssert(x)
#endif
