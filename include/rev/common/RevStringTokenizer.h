/* 
 * File:   RevStringTokenizer.h
 * Author: Kamil
 *
 * Created on 22 sierpień 2012, 16:38
 */

#ifndef REVSTRINGTOKENIZER_H
#define	REVSTRINGTOKENIZER_H

#include <rev/common/RevCommonConfig.h>
#include <vector>
#include <string>
#include <memory>
#include <regex>

namespace rev {

    class StringTokenizer;
    typedef std::shared_ptr<StringTokenizer> StringTokenizerPtr;
    typedef std::shared_ptr<std::vector<std::string> > StringVectorPtr;

    class REVCOMMON_API StringTokenizer {
    protected:
        std::string buffer;
        std::string delimiter;

        StringTokenizer(const std::string& str_, const std::string& delimiter_)
        : buffer(str_), delimiter(delimiter_) {
        }

    public:
        static const std::string WHITE_CHARACTERS;

        static StringTokenizerPtr createTokenizerByCharSet();

        static StringTokenizerPtr
        createTokenizerByCharSet(const std::string& str);

        static StringTokenizerPtr
        createTokenizerByCharSet(const std::string& str, const std::string& charSetDelimiter);

        static StringTokenizerPtr createTokenizerByString();

        static StringTokenizerPtr
        createTokenizerByString(const std::string& str, const std::string& stringDelimiter);

        static StringTokenizerPtr
        createTokenizerByRegex(std::regex_constants::match_flag_type flags = std::regex_constants::match_default);

        static StringTokenizerPtr
        createTokenizerByRegex(const std::string& str, const std::string& regexDelimiter,
                std::regex_constants::match_flag_type flags = std::regex_constants::match_default);
        
        StringVectorPtr split();
        
        static StringVectorPtr 
        splitByCharSet(const std::string& str, const std::string& charSetDelimiter = WHITE_CHARACTERS);

        static StringVectorPtr
        splitByString(const std::string& str, const std::string& stringDelimiter);

        static StringVectorPtr
        splitByRegex(const std::string& str, const std::string& regexDelimiter,
                std::regex_constants::match_flag_type flags = std::regex_constants::match_default);
        

        virtual ~StringTokenizer() {
        }

        virtual void setString(const std::string& str) = 0;
        virtual void setDelimiter(const std::string& delim) = 0;
        virtual void set(const std::string& str, const std::string& stringDelimiter) = 0;
        /**
         * returns "" if there are no more tokens.
         */
        virtual std::string next() = 0;
    };
}

#endif	/* REVSTRINGTOKENIZER_H */

