/*
 * RevSleep.h
 *
 *  Created on: 04-10-2012
 *      Author: Revers
 */

#ifndef REVSLEEP_H_
#define REVSLEEP_H_

#include <rev/common/RevCommonConfig.h>

#ifdef REVCOMMON_WINDOWS
#include <windows.h>
#else
#include <unistd.h>
#endif

namespace rev {

    inline void sleep(int ms) {
#ifdef REVCOMMON_WINDOWS
        Sleep(ms);
#else
        usleep(ms * 1000);
#endif
    }

} // namespace rev

#endif /* REVSLEEP_H_ */
