/* 
 * File:   RevXMLFactoryRegistry.h
 * Author: Kamil
 *
 * Created on 7 sierpień 2012, 17:31
 */

#ifndef REVXMLFACTORYREGISTRY_H
#define	REVXMLFACTORYREGISTRY_H

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevStringUtil.h>
#include <rev/common/RevAssert.h>
#include <vector>
#include <map>
#include <string>

namespace rev {

    class XMLElement;
    typedef XMLElement* (*XMLElementFactory)();
    typedef std::map< std::string, XMLElementFactory > XMLFactories;

    class REVCOMMON_API XMLFactoryRegistry {
        XMLFactories xmlFactories;
        
        XMLFactoryRegistry() {
        }
        XMLFactoryRegistry(const XMLFactoryRegistry& orig);
        XMLFactoryRegistry& operator=(const XMLFactoryRegistry&);
    public:
        static XMLFactoryRegistry& getInstance();

        ~XMLFactoryRegistry() {
        }

        void registerFactory(const char* name, XMLElementFactory factory) {
            REVCOMMON_TRACE("Registering '" << name << "' factory...");
            
            assertMsg(xmlFactories.find(name) == xmlFactories.end(), 
                    "Factory already registered: '" << name << "'!");
            
            xmlFactories[name] = factory;
        }
        
        XMLElementFactory getFactory(const char* name) {
            assertMsg(xmlFactories.find(name) != xmlFactories.end(), 
                    "There is no such factory: '" << name << "'!");
            
            if(xmlFactories.find(name) == xmlFactories.end()) {
                return NULL;
            }
            
            return xmlFactories[name];
        }
    };
    
    class REVCOMMON_API XMLFactoryRegisterObject {
    public:
        XMLFactoryRegisterObject(const char* name, XMLElementFactory factory) {
            XMLFactoryRegistry::getInstance().registerFactory(name, factory);
        }
    };
} // end namespace rev
#endif	/* REVXMLFACTORYREGISTRY_H */

