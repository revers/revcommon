/* 
 * File:   RevXMLElement.h
 * Author: Kamil
 *
 * Created on 7 sierpień 2012, 15:20
 */

#ifndef REVXMLELEMENT_H
#define	REVXMLELEMENT_H


#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevStringUtil.h>
#include <rev/common/RevAssert.h>
#include <list>
#include <map>
#include <string>
#include <ostream>
#include <iterator>
#include "RevXMLFactoryRegistry.h"
#include <memory>

// indent in xml file:
#define REV_SUB_INDENT "  "

namespace pugi {
    class xml_node;
}

namespace rev {

    class XMLElement;

    //--------------------------------------------------------------------------
    // IXMLElementIterator
    //==========================================================================

    class REVCOMMON_API IXMLElementIterator {
    public:

        virtual bool hasNextElement() = 0;
        virtual XMLElement* nextElement() = 0;

        virtual ~IXMLElementIterator() {
        }
    };

    //--------------------------------------------------------------------------
    // Private namespace begin
    //==========================================================================
    namespace prv {

        class AbstractListIteratorBase {
        public:

            virtual ~AbstractListIteratorBase() {
            }

            virtual bool hasNextElement() = 0;
            virtual XMLElement* nextElement() = 0;
        };

        template<typename T>
        class AbstractListIterator : public AbstractListIteratorBase {
            typedef std::list<T*> AbstractList;
            typedef typename AbstractList::iterator AbstractListIter;

            AbstractList* listPtr;
            AbstractListIter iter;
            AbstractListIter endIter;

        public:

            AbstractListIterator(AbstractList* listPtr_) : listPtr(listPtr_) {
                iter = listPtr_->begin();
                endIter = listPtr_->end();
            }

            /**
             * @Override
             */
            bool hasNextElement() {
                iter != endIter;
            }

            /**
             * @Override
             */
            XMLElement* nextElement() {
                assert(iter != endIter);
                T* elem = *iter;
                ++iter;

                return dynamic_cast<XMLElement*> (elem);
            }
        };
    }
    //--------------------------------------------------------------------------
    // Private namespace end
    //==========================================================================

    //--------------------------------------------------------------------------
    // XMLElementIterator
    //==========================================================================

    class REVCOMMON_API XMLElementIterator : public IXMLElementIterator {
        prv::AbstractListIteratorBase* valuePtr;
    public:

        template<typename T>
        XMLElementIterator(std::list<T*>* valuePtr_) :
        valuePtr(new prv::AbstractListIterator<T>(valuePtr_)) {
        }

        ~XMLElementIterator() {
            delete valuePtr;
        }

        /**
         * @Override
         */
        bool hasNextElement() {
            return valuePtr->hasNextElement();
        }

        /**
         * @Override
         */
        XMLElement* nextElement() {
            return valuePtr->nextElement();
        }

    };

    //--------------------------------------------------------------------------
    // Private namespace begin
    //==========================================================================
    namespace prv {

        class XMLAbstractListValueBase {
        public:

            virtual void* getValuePtr() = 0;

            virtual void addElement(XMLElement* ptr) = 0;

            virtual ~XMLAbstractListValueBase() {
            }

            virtual IXMLElementIterator* iterator() = 0;
        };

        template<typename T>
        class XMLAbstractListValue : public XMLAbstractListValueBase {
            typedef std::list<T*> AbstractList;

        public:
            std::list<T*>* valuePtr;

            XMLAbstractListValue(AbstractList* valuePtr_) : valuePtr(valuePtr_) {
            }

            /**
             * @Override
             */
            void* getValuePtr() {
                return (void*) valuePtr;
            }

            /**
             * @Override
             */
            void addElement(XMLElement* ptr) {
                valuePtr->push_back(dynamic_cast<T*> (ptr));
            }

            /**
             * @Override
             */
            IXMLElementIterator* iterator() {
                new XMLElementIterator(valuePtr);
            }
        };

        class XMLAbstractValueBase {
        public:
            virtual void mapStringToValue(const std::string& s) = 0;
            virtual const char* mapValueToString() = 0;
            virtual void* getValuePtr() = 0;

            virtual ~XMLAbstractValueBase() {
            }
        };

        template<typename T>
        class XMLAbstractValue : public XMLAbstractValueBase {
        public:
            T* valuePtr;

            XMLAbstractValue(T* valuePtr_) : valuePtr(valuePtr_) {
            }

            /**
             * @Override
             */
            void mapStringToValue(const std::string& s) {
                *valuePtr = rev::StringUtil::stringTo<T > (s);
            }

            /**
             * @Override
             */
            const char* mapValueToString() {
                return (rev::StringUtil::toString<T > (*valuePtr)).c_str();
            }

            /**
             * @Override
             */
            void* getValuePtr() {
                return (void*) valuePtr;
            }
        };
    }
    //--------------------------------------------------------------------------
    // Private namespace end
    //==========================================================================

    //--------------------------------------------------------------------------
    // XMLValue
    //==========================================================================

    class REVCOMMON_API XMLValue {
        prv::XMLAbstractValueBase* valuePtr;
    public:

        template<typename T>
        XMLValue(T* valuePtr_) :
        valuePtr(new prv::XMLAbstractValue<T>(valuePtr_)) {
        }

        ~XMLValue() {
            delete valuePtr;
        }

        void* getValuePtr() {
            return (void*) valuePtr->getValuePtr();
        }

        const char* getValueString() {
            return valuePtr->mapValueToString();
        }

        void mapStringToValue(const std::string& s) {
            valuePtr->mapStringToValue(s);
        }
    };

    typedef XMLValue XMLAttribute;

    typedef std::map< std::string, rev::XMLElement* > XMLElements;
    typedef std::map< std::string, rev::XMLAttribute* > XMLAttributes;
    typedef std::list< rev::XMLElement* > XMLElementList;

    typedef XMLElement* (*XMLElementFactory)();

    //--------------------------------------------------------------------------
    // XMLElementMetadata
    //==========================================================================

    class REVCOMMON_API XMLElementMetadata {
    public:
        XMLElements* children;
        XMLAttributes* attributes;
        XMLValue* value;
        std::string name;

        XMLElementMetadata() {
            children = NULL;
            attributes = NULL;
            value = NULL;
        }

        ~XMLElementMetadata();

        void print() {
            print(std::cout);
        }

        void print(std::ostream& out) {
            print(out, "");
        }

        void print(std::ostream& out, const std::string& indent);
    };

    //--------------------------------------------------------------------------
    // XMLElement
    //==========================================================================

    class REVCOMMON_API XMLElement {
    private:

        bool inited;
        prv::XMLAbstractListValueBase* siblingElementsPtr;

    public:

        XMLElement() : siblingElementsPtr(NULL) {
            inited = false;
        }

        XMLElement(const std::string& name) : siblingElementsPtr(NULL) {
            meta.name = name;
            inited = false;
        }

        template<typename T>
        XMLElement(const std::string& name, std::list<T*>* childrenElements_) :
        siblingElementsPtr(new prv::XMLAbstractListValue<T>(childrenElements_)) {
        }

        virtual ~XMLElement() {
            if (siblingElementsPtr)
                delete siblingElementsPtr;
        }

        XMLElementMetadata& getMetadata() {
            return meta;
        }

        bool hasMappedElement(const char* elementName) {
            return meta.children != NULL
                    && meta.children->find(elementName) != meta.children->end();
        }

        bool hasMappedAttribute(const char* attrName) {
            return meta.attributes != NULL
                    && meta.attributes->find(attrName) != meta.attributes->end();
        }

        bool hasMappedValue() {
            return meta.value != NULL;
        }

        bool writeToFile(const char* filename);

        bool loadFromFile(const char* filename);

        bool loadFromStream(std::istream& in);

        void print(bool withHeader = true) {
            printToStream(std::cout, withHeader);
        }

        void printToStream(std::ostream& out = std::cout,
                bool withHeader = true);

        virtual const char* getClassName();

    protected:
        XMLElementMetadata meta;

        IXMLElementIterator* siblingIterator() {
            if (siblingElementsPtr) {
                return siblingElementsPtr->iterator();
            }
            return NULL;
        }

        const char* getXMLElementName() {
            return meta.name.c_str();
        }

        void setXMLElementName(const char* name) {
            meta.name = name;
        }

        bool setXMLElementValue(const char* value) {
            if (meta.value == NULL) {
                return false;
            }

            meta.value->mapStringToValue(value);
            return true;
        }

        const char* getXMLElementValue() {
            if (meta.value == NULL) {
                return NULL;
            }

            return meta.value->getValueString();
        }

        bool setXMLElementAttribute(const char* attrName, const char* value) {
            if (!hasMappedAttribute(attrName)) {
                return false;
            }

            XMLAttributes& attrs = *(meta.attributes);
            attrs[attrName]->mapStringToValue(value);
            return true;
        }
        const char* getXMLElementAttribute(const char* attr);

        XMLElement* getXMLChildMetaElement(const char* name) {
            if (!hasMappedElement(name)) {
                return NULL;
            }

            XMLElements& elems = *(meta.children);
            return elems[name];
        }

        virtual void mapXMLTagsToMembers() {
        }

        virtual void setDefaultXMLElementName() {
        }

        virtual void initXMLElement() {
            if (!inited) {
                mapXMLTagsToMembers();
                setDefaultXMLElementName();
                inited = true;
            }
        }

        template<typename T>
        void mapElement(std::list<T*>* memberVectorPtr, const char* elementName) {
            assert(memberVectorPtr);

            if (!meta.children) {
                meta.children = new XMLElements();
            }

            std::string elementNameStr = elementName;

            T* e = new T(elementNameStr, memberVectorPtr);
            // Just in case. Works only once:
            e->initXMLElement();

            XMLElements& elems = *(meta.children);
            elems[elementNameStr] = e;
        }

        template<typename T>
        void mapValue(T* memberPtr) {
            assert(!meta.value);

            meta.value = new XMLValue(memberPtr);
        }

        template<typename T>
        void mapAttribute(T* memberPtr, const char* attrName) {
            if (!meta.attributes) {
                meta.attributes = new XMLAttributes();
            }

            XMLAttributes& attrs = *(meta.attributes);
            attrs[attrName] = new XMLAttribute(memberPtr);
        }

    private:
        bool load(XMLElement* element, pugi::xml_node* node);

        void printToStream(XMLElement* element, std::ostream& out, const std::string& indent);
    };
}
//--------------------------------------------------------------------------
// Defines
//==========================================================================
#define REV_DECLARE_XML_FACTORY(className, xmlTagName)	\
        template<typename TTTT> \
        className(const std::string& name, std::list<TTTT*>* childrenElements_) : \
        XMLElement(name, childrenElements_) {} \
        static const char* getStaticElementName(); \
        static const char* getStaticClassName(); \
        static rev::XMLElement* staticCreateElement(); \
        static rev::XMLElementFactory getStaticElementFactory(); \
        void setDefaultXMLElementName() { \
                setXMLElementName(className::getStaticElementName()); \
        } \
        const char* getClassName() { \
                return className::getStaticClassName();\
        }


#define REV_IMPLEMENT_XML_FACTORY(className, xmlTagName) \
        const char* className::getStaticElementName() { \
            static const char* name = xmlTagName; \
            return name; \
        } \
        const char* className::getStaticClassName() { \
            static const char* name = #className; \
            return name; \
        } \
        rev::XMLElement* className::staticCreateElement(){ \
                return new className(); \
        } \
        rev::XMLElementFactory className::getStaticElementFactory() { \
            return className::staticCreateElement; \
        } \
        rev::XMLFactoryRegisterObject regObject_##className ( \
                        xmlTagName "@" #className, \
                        className::getStaticElementFactory());




#endif	/* REVXMLELEMENT_H */

