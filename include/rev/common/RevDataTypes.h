/* 
 * File:   RevDataTypes.h
 * Author: Kamil
 *
 * Created on 30 sierpień 2012, 15:50
 */

#ifndef REVDATATYPES_H
#define	REVDATATYPES_H

#include <iostream>

typedef unsigned int uint;

namespace rev {

	template<typename T>
	struct TVec2 {
		typedef T base_type;
		union {
			T x;
			T s0;
		};

		union {
			T y;
			T s1;
		};

		TVec2() :
				x(0), y(0) {
		}

		TVec2(T val) :
				x(val), y(val) {
		}

		TVec2(T x, T y) :
				x(x), y(y) {
		}

		inline rev::TVec2<T>& operator=(const base_type& val) {
			x = val;
			y = val;

			return *this;
		}
	};

	template<typename T>
	struct TVec3 {
		typedef T base_type;
		union {
			T x;
			T s0;
		};

		union {
			T y;
			T s1;
		};

		union {
			T z;
			T s2;
		};

		TVec3() :
				x(0), y(0), z(0) {
		}

		TVec3(T val) :
				x(val), y(val), z(val) {
		}

		TVec3(T x, T y, T z) :
				x(x), y(y), z(z) {
		}

		inline rev::TVec3<T>& operator=(const base_type& val) {
			x = val;
			y = val;
			z = val;

			return *this;
		}
	};

	template<typename T>
	struct TVec4 {
		typedef T base_type;
		union {
			T x;
			T s0;
		};

		union {
			T y;
			T s1;
		};

		union {
			T z;
			T s2;
		};

		union {
			T w;
			T s3;
		};

		TVec4() :
				x(0), y(0), z(0), w(0) {
		}

		TVec4(T val) :
				x(val), y(val), z(val), w(val) {
		}

		TVec4(T x, T y, T z, T w) :
				x(x), y(y), z(z), w(w) {
		}

		inline rev::TVec4<T>& operator=(const base_type& val) {
			x = val;
			y = val;
			z = val;
			w = val;

			return *this;
		}
	};

	typedef int rev_int;
	typedef unsigned int rev_uint;
	typedef float rev_float;
	typedef double rev_double;

//	template<typename T>
//	T abs(const T& val) {
//		return val < 0 ? -val : val;
//	}

	typedef TVec2<rev_float> vec2f;
	typedef TVec2<rev_double> vec2d;
	typedef TVec2<rev_int> vec2i;
	typedef TVec2<rev_uint> vec2ui;

	typedef TVec3<rev_float> vec3f;
	typedef TVec3<rev_double> vec3d;
	typedef TVec3<rev_int> vec3i;
	typedef TVec3<rev_uint> vec3ui;

	typedef TVec4<rev_float> vec4f;
	typedef TVec4<rev_double> vec4d;
	typedef TVec4<rev_int> vec4i;
	typedef TVec4<rev_uint> vec4ui;

	struct vector1_type {
		static vector1_type create() {
			return vector1_type();
		}
	};

	struct vector2_type {
		static vector2_type create() {
			return vector2_type();
		}
	};

	struct vector3_type {
		static vector3_type create() {
			return vector3_type();
		}
	};

	struct vector4_type {
		static vector4_type create() {
			return vector4_type();
		}
	};

	template<typename T>
	struct type_traits {
	};

#define DECLARE_1D_TYPE_TRAITS(X)	\
template<> \
struct type_traits<X> { \
	typedef X base_type; \
	typedef vector1_type vector_type; \
}

#define DECLARE_2D_TYPE_TRAITS(X)	\
template<> \
struct type_traits<X> { \
	typedef X base_type; \
	typedef vector2_type vector_type; \
}

#define DECLARE_3D_TYPE_TRAITS(X)	\
template<> \
struct type_traits<X> { \
	typedef X base_type; \
	typedef vector3_type vector_type; \
}

#define DECLARE_4D_TYPE_TRAITS(X)	\
template<> \
struct type_traits<X> { \
	typedef X base_type; \
	typedef vector4_type vector_type; \
}

#define DECLARE_1D_TYPE_TRAITS_VEC(X)	\
template<> \
struct type_traits<X> { \
	typedef typename X::base_type base_type; \
	typedef vector1_type vector_type; \
}

#define DECLARE_2D_TYPE_TRAITS_VEC(X)	\
template<> \
struct type_traits<X> { \
	typedef typename X::base_type base_type; \
	typedef vector2_type vector_type; \
}

#define DECLARE_3D_TYPE_TRAITS_VEC(X)	\
template<> \
struct type_traits<X> { \
	typedef typename X::base_type base_type; \
	typedef vector3_type vector_type; \
}

#define DECLARE_4D_TYPE_TRAITS_VEC(X)	\
template<> \
struct type_traits<X> { \
	typedef typename X::base_type base_type; \
	typedef vector4_type vector_type; \
}

	DECLARE_1D_TYPE_TRAITS(rev_int);
DECLARE_1D_TYPE_TRAITS	(rev_uint);
	DECLARE_1D_TYPE_TRAITS(rev_float);
DECLARE_1D_TYPE_TRAITS	(rev_double);

DECLARE_1D_TYPE_TRAITS(signed short);
DECLARE_1D_TYPE_TRAITS  (unsigned short);
    DECLARE_1D_TYPE_TRAITS(signed char);
DECLARE_1D_TYPE_TRAITS  (unsigned char);

	DECLARE_2D_TYPE_TRAITS_VEC(vec2f);
DECLARE_2D_TYPE_TRAITS_VEC	(vec2d);
	DECLARE_2D_TYPE_TRAITS_VEC(vec2i);
DECLARE_2D_TYPE_TRAITS_VEC	(vec2ui);

	DECLARE_3D_TYPE_TRAITS_VEC(vec3f);
DECLARE_3D_TYPE_TRAITS_VEC	(vec3d);
	DECLARE_3D_TYPE_TRAITS_VEC(vec3i);
DECLARE_3D_TYPE_TRAITS_VEC	(vec3ui);

	DECLARE_4D_TYPE_TRAITS_VEC(vec4f);
DECLARE_4D_TYPE_TRAITS_VEC	(vec4d);
	DECLARE_4D_TYPE_TRAITS_VEC(vec4i);
DECLARE_4D_TYPE_TRAITS_VEC	(vec4ui);

	namespace prv {

		template<typename T>
		bool componentsGreaterImplVec(const TVec2<T>& vec,
				const typename rev::type_traits<T>::base_type& value) {
			return vec.x > value || vec.y > value;
		}

		template<typename T>
		bool componentsGreaterImplVec(const TVec3<T>& vec,
				const typename rev::type_traits<T>::base_type& value) {
			return vec.x > value || vec.y > value || vec.z > value;
		}

		template<typename T>
		bool componentsGreaterImplVec(const TVec4<T>& vec,
				const typename rev::type_traits<T>::base_type& value) {
			return vec.x > value || vec.y > value || vec.z > value || vec.w > value;
		}

		template<typename T>
		bool componentsGreaterImpl(const T& vec,
				const typename rev::type_traits<T>::base_type& value,
				std::false_type) {
			return componentsGreaterImplVec(vec, value);
		}

		template<typename T>
		bool componentsGreaterImpl(const T& vec,
				const typename rev::type_traits<T>::base_type& value,
				std::true_type) {
			return vec > value;
		}

		template<typename T>
		TVec2<T> absoluteImplVec(const TVec2<T>& vec) {
			typename TVec2<T>::base_type x = vec.x < 0 ? -vec.x : vec.x;
			typename TVec2<T>::base_type y = vec.y < 0 ? -vec.y : vec.y;

			return { {x}, {y}};
		}

		template<typename T>
		TVec3<T> absoluteImplVec(const TVec3<T>& vec) {
			typename TVec3<T>::base_type x = vec.x < 0 ? -vec.x : vec.x;
			typename TVec3<T>::base_type y = vec.y < 0 ? -vec.y : vec.y;
			typename TVec3<T>::base_type z = vec.z < 0 ? -vec.z : vec.z;

			return { {x}, {y}, {z}};
		}

		template<typename T>
		TVec4<T> absoluteImplVec(const TVec4<T>& vec) {
			typename TVec4<T>::base_type x = vec.x < 0 ? -vec.x : vec.x;
			typename TVec4<T>::base_type y = vec.y < 0 ? -vec.y : vec.y;
			typename TVec4<T>::base_type z = vec.z < 0 ? -vec.z : vec.z;
			typename TVec4<T>::base_type w = vec.w < 0 ? -vec.w : vec.w;

			return { {x}, {y}, {z}, {w}};
		}

		template<typename T>
		T absoluteImpl(const T& vec, std::false_type) {
			return absoluteImplVec(vec);
		}

		template<typename T>
		T absoluteImpl(const T& val, std::true_type) {
			return val < 0 ? -val : val;
		}
	}

	template<typename T>
	rev_double componentsGreater(const T& vec,
			const typename rev::type_traits<T>::base_type& value) {
		return prv::componentsGreaterImpl(vec, value, std::is_fundamental<T>());
	}

	template<typename T>
	T abs(const T& val) {
		return prv::absoluteImpl(val, std::is_fundamental<T>());
	}
}

template<typename T>
inline bool operator==(const rev::TVec2<T>& vec1, const rev::TVec2<T>& vec2) {
	return vec1.x == vec2.x && vec1.y == vec2.y;
}

template<typename T>
inline bool operator==(const rev::TVec3<T>& vec1, const rev::TVec3<T>& vec2) {
	return vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z;
}

template<typename T>
inline bool operator==(const rev::TVec4<T>& vec1, const rev::TVec4<T>& vec2) {
	return vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z
			&& vec1.w == vec2.w;
}

template<typename T>
inline bool operator!=(const rev::TVec2<T>& vec1, const rev::TVec2<T>& vec2) {
	return vec1.x != vec2.x || vec1.y != vec2.y;
}

template<typename T>
inline bool operator!=(const rev::TVec3<T>& vec1, const rev::TVec3<T>& vec2) {
	return vec1.x != vec2.x || vec1.y != vec2.y || vec1.z != vec2.z;
}

template<typename T>
inline bool operator!=(const rev::TVec4<T>& vec1, const rev::TVec4<T>& vec2) {
	return vec1.x != vec2.x || vec1.y != vec2.y || vec1.z != vec2.z
			|| vec1.w != vec2.w;
}

template<typename T>
inline std::ostream& operator<<(std::ostream& out, const rev::TVec2<T>& vec) {
	return out << "(" << vec.x << "," << vec.y << ")";
}

template<typename T>
inline std::ostream& operator<<(std::ostream& out, const rev::TVec3<T>& vec) {
	return out << "(" << vec.x << "," << vec.y << "," << vec.z << ")";
}

template<typename T>
inline std::ostream& operator<<(std::ostream& out, const rev::TVec4<T>& vec) {
	return out << "(" << vec.x << "," << vec.y << "," << vec.z << "," << vec.w
			<< ")";
}

template<typename T>
inline rev::TVec2<T> operator-(const rev::TVec2<T>& vec) {
	return { {-vec.x}, {-vec.y}};
}

template<typename T>
inline rev::TVec3<T> operator-(const rev::TVec3<T>& vec) {
	return { {-vec.x}, {-vec.y}, {-vec.z}};
}

template<typename T>
inline rev::TVec4<T> operator-(const rev::TVec4<T>& vec) {
	return { {-vec.x}, {-vec.y}, {-vec.z}, {-vec.w}};
}

template<typename T>
inline rev::TVec2<T> operator+(const rev::TVec2<T>& vec1,
		const rev::TVec2<T>& vec2) {
	return { {vec1.x + vec2.x}, {vec1.y + vec2.y}};
}

template<typename T>
inline rev::TVec3<T> operator+(const rev::TVec3<T>& vec1,
		const rev::TVec3<T>& vec2) {
	return { {vec1.x + vec2.x}, {vec1.y + vec2.y}, {vec1.z + vec2.z}};
}

template<typename T>
inline rev::TVec4<T> operator+(const rev::TVec4<T>& vec1,
		const rev::TVec4<T>& vec2) {
	return { {vec1.x + vec2.x}, {vec1.y + vec2.y}, {vec1.z + vec2.z}, {vec1.w + vec2.w}};
}

template<typename T>
inline rev::TVec2<T> operator-(const rev::TVec2<T>& vec1,
		const rev::TVec2<T>& vec2) {
	return { {vec1.x - vec2.x}, {vec1.y - vec2.y}};
}

template<typename T>
inline rev::TVec3<T> operator-(const rev::TVec3<T>& vec1,
		const rev::TVec3<T>& vec2) {
	return { {vec1.x - vec2.x}, {vec1.y - vec2.y}, {vec1.z - vec2.z}};
}

template<typename T>
inline rev::TVec4<T> operator-(const rev::TVec4<T>& vec1,
		const rev::TVec4<T>& vec2) {
	return { {vec1.x - vec2.x}, {vec1.y - vec2.y}, {vec1.z - vec2.z}, {vec1.w - vec2.w}};
}

template<typename T>
inline rev::TVec2<T> operator*(const rev::TVec2<T>& vec1,
		const rev::TVec2<T>& vec2) {
	return { {vec1.x * vec2.x}, {vec1.y * vec2.y}};
}

template<typename T>
inline rev::TVec3<T> operator*(const rev::TVec3<T>& vec1,
		const rev::TVec3<T>& vec2) {
	return { {vec1.x * vec2.x}, {vec1.y * vec2.y}, {vec1.z * vec2.z}};
}

template<typename T>
inline rev::TVec4<T> operator*(const rev::TVec4<T>& vec1,
		const rev::TVec4<T>& vec2) {
	return { {vec1.x * vec2.x}, {vec1.y * vec2.y}, {vec1.z * vec2.z}, {vec1.w * vec2.w}};
}

template<typename T>
inline rev::TVec2<T> operator/(const rev::TVec2<T>& vec1,
		const rev::TVec2<T>& vec2) {
	return { {vec1.x / vec2.x}, {vec1.y / vec2.y}};
}

template<typename T>
inline rev::TVec3<T> operator/(const rev::TVec3<T>& vec1,
		const rev::TVec3<T>& vec2) {
	return { {vec1.x / vec2.x}, {vec1.y / vec2.y}, {vec1.z / vec2.z}};
}

template<typename T>
inline rev::TVec4<T> operator/(const rev::TVec4<T>& vec1,
		const rev::TVec4<T>& vec2) {
	return { {vec1.x / vec2.x}, {vec1.y / vec2.y}, {vec1.z / vec2.z}, {vec1.w / vec2.w}};
}

template<typename T>
inline rev::TVec2<T> operator/(const rev::TVec2<T>& vec1,
		const typename rev::TVec2<T>::base_type& val) {
	return { {vec1.x / val}, {vec1.y / val}};
}

template<typename T>
inline rev::TVec3<T> operator/(const rev::TVec3<T>& vec1,
		const typename rev::TVec3<T>::base_type& val) {
	return { {vec1.x / val}, {vec1.y / val}, {vec1.z / val}};
}

template<typename T>
inline rev::TVec4<T> operator/(const rev::TVec4<T>& vec1,
		const typename rev::TVec4<T>::base_type& val) {
	return { {vec1.x / val}, {vec1.y / val}, {vec1.z / val}, {vec1.w / val}};
}

template<typename T>
inline rev::TVec2<T> operator+(const rev::TVec2<T>& vec1,
		const typename rev::TVec2<T>::base_type& val) {
	return { {vec1.x + val}, {vec1.y + val}};
}

template<typename T>
inline rev::TVec3<T> operator+(const rev::TVec3<T>& vec1,
		const typename rev::TVec3<T>::base_type& val) {
	return { {vec1.x + val}, {vec1.y + val}, {vec1.z + val}};
}

template<typename T>
inline rev::TVec4<T> operator+(const rev::TVec4<T>& vec1,
		const typename rev::TVec4<T>::base_type& val) {
	return { {vec1.x + val}, {vec1.y + val}, {vec1.z + val}, {vec1.w + val}};
}

template<typename T>
inline rev::TVec2<T> operator-(const rev::TVec2<T>& vec1,
		const typename rev::TVec2<T>::base_type& val) {
	return { {vec1.x - val}, {vec1.y - val}};
}

template<typename T>
inline rev::TVec3<T> operator-(const rev::TVec3<T>& vec1,
		const typename rev::TVec3<T>::base_type& val) {
	return { {vec1.x - val}, {vec1.y - val}, {vec1.z - val}};
}

template<typename T>
inline rev::TVec4<T> operator-(const rev::TVec4<T>& vec1,
		const typename rev::TVec4<T>::base_type& val) {
	return { {vec1.x - val}, {vec1.y - val}, {vec1.z - val}, {vec1.w - val}};
}

template<typename T>
inline rev::TVec2<T> operator*(const rev::TVec2<T>& vec1,
		const typename rev::TVec2<T>::base_type& val) {
	return { {vec1.x * val}, {vec1.y * val}};
}

template<typename T>
inline rev::TVec3<T> operator*(const rev::TVec3<T>& vec1,
		const typename rev::TVec3<T>::base_type& val) {
	return { {vec1.x * val}, {vec1.y * val}, {vec1.z * val}};
}

template<typename T>
inline rev::TVec4<T> operator*(const rev::TVec4<T>& vec1,
		const typename rev::TVec4<T>::base_type& val) {
	return { {vec1.x * val}, {vec1.y * val}, {vec1.z * val}, {vec1.w * val}};
}

#endif	/* REVDATATYPES_H */

