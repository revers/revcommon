#ifndef REVCOMMON_CONFIG_H
#define	REVCOMMON_CONFIG_H

#define REVCOMMON_VERSION_MAJOR 0
#define REVCOMMON_VERSION_MINOR 1 

#ifndef REVCOMMON_STATIC
/* #undef REVCOMMON_STATIC */
#endif

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64)
#define REVCOMMON_WINDOWS
#endif

#if defined(REVCOMMON_WINDOWS)
#   define REVCOMMON_CDECL_CALL    __cdecl
#   define REVCOMMON_STD_CALL      __stdcall
#   define REVCOMMON_CALL          REVCOMMON_CDECL_CALL
#   define REVCOMMON_EXPORT_API    __declspec(dllexport)
#   define REVCOMMON_IMPORT_API    __declspec(dllimport)
#else
#   define REVCOMMON_CDECL_CALL
#   define REVCOMMON_STD_CALL
#   define REVCOMMON_CALL
#   define REVCOMMON_EXPORT_API
#   define REVCOMMON_IMPORT_API
#endif

#if defined REVCOMMON_EXPORTS
#   define REVCOMMON_API REVCOMMON_EXPORT_API
#elif defined REVCOMMON_STATIC
#   define REVCOMMON_API
#   if defined(_MSC_VER) && !defined(REVCOMMON_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevCommonStatic64")
#       else
#           pragma comment(lib, "RevCommonStatic")
#       endif
#   endif
#else
#   define REVCOMMON_API REVCOMMON_IMPORT_API
#   if defined(_MSC_VER) && !defined(REVCOMMON_NO_LIB_PRAGMA)
#       ifdef _WIN64
#           pragma comment(lib, "RevCommon64")
#       else
#           pragma comment(lib, "RevCommon")
#       endif
#   endif
#endif

#ifndef REVCOMMON_ERR_MSG_PRINT_TIME
/* #undef REVCOMMON_ERR_MSG_PRINT_TIME */
#endif

#ifndef REVCOMMON_ERR_STREAM_TO_FILE
#define REVCOMMON_ERR_STREAM_TO_FILE
#endif

#define REVCOMMON_ERROR_FILE "error-log.txt"

#ifdef NDEBUG
#define REVCOMMON_TRACE(x) ((void)0)
#else
#include <iostream>
#define REVCOMMON_TRACE(x) std::cout << x << std::endl
#endif

#define REVCOMMON_ERR_MSG(x) REV_ERROR_MSG(x)

//define REVCOMMON_NO_COLORIZED_OUT

#endif /* REVCOMMON_CONFIG_H */
