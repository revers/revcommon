/* 
 * File:   RevStringUtil.h
 * Author: Revers
 *
 * Created on 4 sierpień 2012, 10:45
 */

#ifndef REVSTRINGUTIL_H
#define	REVSTRINGUTIL_H

#include <rev/common/RevCommonConfig.h>
#include <string>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <stdarg.h>

namespace rev {

    class REVCOMMON_API StringUtil {
    public:

        static std::string removeWhitespaces(const std::string& s) {
            std::string str(s);
            str.erase(std::remove_if(str.begin(), str.end(), ::isspace), str.end());
            return str;
        }

        template <typename T>
        static std::string toString(T val) {
            std::ostringstream out;
            out << val;
            return out.str();
        }

        template <typename T>
        static T stringTo(const std::string& str) {
            T output;
            std::istringstream in(str);
            in >> output;
            return output;
        }

        static bool isUnsignedInteger(const std::string& s) {
            std::string::const_iterator it = s.begin();

            while (it != s.end() && std::isdigit(*it)) {
                ++it;
            }

            return !s.empty() && it == s.end();
        }

        static bool isSignedInteger(const std::string& s) {
            std::string::const_iterator it = s.begin();

            if (*it == '-') {
                ++it;
            }

            while (it != s.end() && std::isdigit(*it)) {
                ++it;
            }

            return !s.empty() && it == s.end();
        }

        static bool startsWith(const std::string& str, const std::string& prefix) {
            return str.size() >= prefix.size() &&
                    str.substr(0, prefix.size()) == prefix;
        }

        static bool startsWith(const std::string& str, const char* prefix) {
            int sz = strlen(prefix);
            return str.size() >= sz && str.substr(0, sz) == prefix;
        }

        static std::string replaceStr(const std::string& src, const std::string& target, const std::string& replacement);

        static std::string replaceChar(const std::string& src, char target, char replacement) {
            std::string s = src;
            std::replace(s.begin(), s.end(), target, replacement);
            return s;
        }

        static std::string toLower(const std::string& str) {
            std::string result = str;

            std::transform(result.begin(), result.end(), result.begin(), ::tolower);

            return result;
        }

        static std::string toUpper(const std::string& str) {
            std::string result = str;

            std::transform(result.begin(), result.end(), result.begin(), ::toupper);

            return result;
        }

        static std::wstring toWideString(const std::string& utf8string);

        static std::string fromWideString(const std::wstring& widestring);

        static std::string trim(std::string const& source,  const char* delims = " \t\r\n") {
            std::string result(source);
            std::string::size_type index = result.find_last_not_of(delims);
            if (index != std::string::npos)
                result.erase(++index);

            index = result.find_first_not_of(delims);
            if (index != std::string::npos)
                result.erase(0, index);
            else
                result.erase();
            return result;
        }
		
		static std::string format(const std::string& fmt, ...);
    };

    template <>
    inline std::string StringUtil::toString<bool>(bool val) {
        return val ? "true" : "false";
    }

    template <>
    inline bool StringUtil::stringTo<bool>(const std::string& str) {
        return str == "true" ? true : false;
    }

    template <>
	inline std::string StringUtil::toString<std::string>(std::string val) {
		return val;
	}

    template <>
	inline std::string StringUtil::toString<const char*>(const char* val) {
		return val;
	}

    template <>
	inline std::string StringUtil::stringTo<std::string>(const std::string& str) {
		return str;
	}

    template <>
	inline const char* StringUtil::stringTo<const char*>(const std::string& str) {
		return str.c_str();
	}

} /* namespace rev */


#endif	/* REVSTRINGUTIL_H */

