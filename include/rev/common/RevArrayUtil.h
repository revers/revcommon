/*
 * RevArrayUtil.h
 *
 *  Created on: 02-09-2012
 *      Author: Revers
 */

#ifndef REVARRAYUTIL_H_
#define REVARRAYUTIL_H_

#include <rev/common/RevCommonConfig.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevArray.h>
#include <rev/common/RevDataGenerator.h>

namespace rev {

	class ArrayUtil {
	public:

		template<typename T>
		static bool equals(ArrayWrapper1D<T>& array1, ArrayWrapper1D<T>& array2) {
			assertMsg(array1.getSize() == array2.getSize(),
					"ERROR: " << rev::a2s(R(array1.getSize()), R(array2.getSize())));

			for (int i = 0; i < array1.getSize(); i++) {
				T& val1 = array1(i);
				T& val2 = array2(i);

				if (val1 != val2) {
					REVCOMMON_ERR_MSG(
							"ARRAYS NOT EQUAL (val1 != val2); " << rev::a2s(R(val1), R(val2)) << "; index = " << i << "; size = " << array1.getSize());
					return false;
				}
			}

			return true;
		}

		template<typename T>
		static bool equals(T* array1, T* array2, int size) {
			ArrayWrapper1D<T> arr1(array1, size);
			ArrayWrapper1D<T> arr2(array2, size);

			return equals<T>(arr1, arr2);
		}

		template<typename T>
		static bool equals(ArrayWrapper2D<T>& array1, ArrayWrapper2D<T>& array2) {
			assertMsg(array1.getSize() == array2.getSize(),
					"ERROR: " << rev::a2s(R(array1.getSize()), R(array2.getSize())));

			assertMsg(
					array1.getWidth() == array2.getWidth() && array1.getHeight() == array2.getHeight(),
					"ERROR: " << rev::a2s(R(array1.getWidth()), R(array2.getWidth()), R(array1.getHeight()), R(array2.getHeight())));

			return equals<T>(array1.data(), array2.data(), array1.getSize());
		}

		template<typename T>
		static bool equals(ArrayWrapper3D<T>& array1, ArrayWrapper3D<T>& array2) {
			assertMsg(array1.getSize() == array2.getSize(),
					"ERROR: " << rev::a2s(R(array1.getSize()), R(array2.getSize())));

			assertMsg(
					array1.getWidth() == array2.getWidth() && array1.getHeight() == array2.getHeight() && array1.getDepth() == array2.getDepth(),
					"ERROR: " << rev::a2s(R(array1.getWidth()), R(array2.getWidth()), R(array1.getHeight()), R(array2.getHeight()), R(array1.getDepth()), R(array2.getDepth())));

			return equals<T>(array1.data(), array2.data(), array1.getSize());
		}

		template<typename T>
		static bool equalsApproximately(ArrayWrapper1D<T>& array1,
				ArrayWrapper1D<T>& array2,
				typename rev::type_traits<T>::base_type maxDiff, T* totalDiff =
						nullptr, T* meanDiff = nullptr) {

			assertMsg(array1.getSize() == array2.getSize(),
					"ERROR: " << rev::a2s(R(array1.getSize()), R(array2.getSize())));

			T absoluteDifference = 0;

			for (int i = 0; i < array1.getSize(); i++) {
				T& val1 = array1(i);
				T& val2 = array2(i);

				absoluteDifference = absoluteDifference + rev::abs(val2 - val1);
			}

			if (totalDiff) {
				*totalDiff = absoluteDifference;
			}

			T meanDifference = absoluteDifference / array1.getSize();
                        
                        if(meanDiff) {
                            *meanDiff = meanDifference;
                        }

			if (rev::componentsGreater(meanDifference, maxDiff)) {
				REVCOMMON_ERR_MSG(
						"ARRAYS NOT EQUAL APPROXIMATELY (array1 !~ array2, meanDifference > maxDiff); " << rev::a2s(R(absoluteDifference), R(meanDifference), R(maxDiff), R(array1.getSize()), R(array2.getSize())));

				return false;
			}

			return true;
		}

		template<typename T>
		static bool equalsApproximately(T* array1, T* array2, int size,
				typename rev::type_traits<T>::base_type maxDiff, T* totalDiff =
						nullptr, T* meanDiff = nullptr) {
			ArrayWrapper1D<T> arr1(array1, size);
			ArrayWrapper1D<T> arr2(array2, size);

			return equalsApproximately<T>(arr1, arr2, maxDiff, totalDiff, meanDiff);
		}

		template<typename T>
		static bool equalsApproximately(ArrayWrapper2D<T>& array1,
				ArrayWrapper2D<T>& array2,
				typename rev::type_traits<T>::base_type maxDiff, T* totalDiff =
						nullptr, T* meanDiff = nullptr) {

			assertMsg(array1.getSize() == array2.getSize(),
					"ERROR: " << rev::a2s(R(array1.getSize()), R(array2.getSize())));

			assertMsg(
					array1.getWidth() == array2.getWidth() && array1.getHeight() == array2.getHeight(),
					"ERROR: " << rev::a2s(R(array1.getWidth()), R(array2.getWidth()), R(array1.getHeight()), R(array2.getHeight())));

			return equalsApproximately<T>(array1.data(), array2.data(),
					array1.getSize(), maxDiff, totalDiff, meanDiff);
		}

		template<typename T>
		static bool equalsApproximately(ArrayWrapper3D<T>& array1,
				ArrayWrapper3D<T>& array2,
				typename rev::type_traits<T>::base_type maxDiff, T* totalDiff =
						nullptr, T* meanDiff = nullptr) {
			assertMsg(array1.getSize() == array2.getSize(),
					"ERROR: " << rev::a2s(R(array1.getSize()), R(array2.getSize())));

			assertMsg(
					array1.getWidth() == array2.getWidth() && array1.getHeight() == array2.getHeight() && array1.getDepth() == array2.getDepth(),
					"ERROR: " << rev::a2s(R(array1.getWidth()), R(array2.getWidth()), R(array1.getHeight()), R(array2.getHeight()), R(array1.getDepth()), R(array2.getDepth())));

			return equalsApproximately<T>(array1.data(), array2.data(),
					array1.getSize(), maxDiff, totalDiff, meanDiff);
		}

		template<typename T>
		static void fillArray1D(T* ptr, int width,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int size) {

			ArrayWrapper1D<T> array(ptr, width);

			fiilVec<T>(array.begin(), array.end(), min, max, array.getType());
		}

		template<typename T>
		static void fillArray1D(T* ptr, int width,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			fillArray1D<T>(ptr, width, min, max, (int) time(0));
		}

		template<typename T>
		static void fillArray1D(ArrayWrapper1D<T>& array,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {

			fiilVec<T>(array.begin(), array.end(), min, max, seed, array.getType());
		}

		template<typename T>
		static void fillArray1D(ArrayWrapper1D<T>& array,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			fillArray1D<T>(array, min, max, (int) time(0));
		}

		template<typename T>
		static void fillArray2D(T* ptr, int width, int height,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {
			ArrayWrapper2D<T> array(ptr, width, height);

			fiilVec<T>(array.begin(), array.end(), min, max, seed, array.getType());
		}

		template<typename T>
		static void fillArray2D(T* ptr, int width, int height,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			fillArray2D<T>(ptr, width, height, min, max, (int) time(0));
		}

		template<typename T>
		static void fillArray2D(ArrayWrapper2D<T>& array,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {

			fiilVec<T>(array.begin(), array.end(), min, max, seed, array.getType());
		}

		template<typename T>
		static void fillArray2D(ArrayWrapper2D<T>& array,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			fillArray2D<T>(array, min, max);
		}

		template<typename T>
		static void fillArray3D(T* ptr, int width, int height, int depth,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {
			ArrayWrapper3D<T> array(ptr, width, height, depth);

			fiilVec<T>(array.begin(), array.end(), min, max, seed, array.getType());
		}

		template<typename T>
		static void fillArray3D(T* ptr, int width, int height, int depth,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {

			fillArray3D<T>(ptr, width, height, depth, min, max, (int) time(0));
		}

		template<typename T>
		static void fillArray3D(ArrayWrapper3D<T>& array,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {

			fiilVec<T>(array.begin(), array.end(), min, max, seed, array.getType());
		}

		template<typename T>
		static void fillArray3D(ArrayWrapper3D<T>& array,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			return fillArray3D(array, min, max, (int) time(0));
		}

		template<typename T>
		static std::shared_ptr<ArrayWrapper1D<T> > generateArray1D(int width,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {

			return generateArray1D<T>(width, min, max, (int) time(0));
		}

		template<typename T>
		static std::shared_ptr<ArrayWrapper1D<T> > generateArray1D(int width,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {

			typedef ArrayWrapper1D<T> ArrayType;
			typedef std::shared_ptr<ArrayType> ArrayTypePtr;

			ArrayTypePtr vecPtr(new Array1D<T>(width));

			fiilVec<T>(vecPtr->begin(), vecPtr->end(), min, max, seed,
					vecPtr->getType());

			return vecPtr;
		}

		template<typename T>
		static std::shared_ptr<ArrayWrapper2D<T> > generateArray2D(int width,
				int height, typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {

			typedef ArrayWrapper2D<T> ArrayType;
			typedef std::shared_ptr<ArrayType> ArrayTypePtr;

			ArrayTypePtr vecPtr(new Array2D<T>(width, height));

			fiilVec<T>(vecPtr->begin(), vecPtr->end(), min, max, seed,
					vecPtr->getType());

			return vecPtr;
		}

		template<typename T>
		static std::shared_ptr<ArrayWrapper2D<T> > generateArray2D(int width,
				int height, typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			return generateArray2D<T>(width, height, min, max, (int) time(0));
		}

		template<typename T>
		static std::shared_ptr<ArrayWrapper3D<T> > generateArray3D(int width,
				int height, int depth,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed) {

			typedef ArrayWrapper3D<T> ArrayType;
			typedef std::shared_ptr<ArrayType> ArrayTypePtr;

			ArrayTypePtr vecPtr(new Array3D<T>(width, height, depth));

			fiilVec<T>(vecPtr->begin(), vecPtr->end(), min, max, seed,
					vecPtr->getType());

			return vecPtr;
		}

		template<typename T>
		static std::shared_ptr<ArrayWrapper3D<T> > generateArray3D(int width,
				int height, int depth,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max) {
			return generateArray3D<T>(width, height, depth, min, max, (int) time(0));
		}

		/**
		 * inArr should be array of size complexSize * 2;
		 * outArr should be array of size complexSize * 2;
		 *
		 * inArr should be in the following format:
		 *	[x0.re, x0.im, x1.re, x1.im, ..., xn.re, xn.im].
		 *
		 * Then after calling this function the outArr would be:
		 * [ x0.re, x1.re, ..., xn.re,
		 *   x0.im, x1.im, ..., xn.im ].
		 */
		template<typename T>
		static void flattenComplexArray(const T* inArr, T* outArr,
				int complexSize) {
			for (int i = 0; i < complexSize; i++) {
				outArr[i] = inArr[i * 2];
				outArr[complexSize + i] = inArr[i * 2 + 1];
			}
		}

		/**
		 * inArr should be array of size complexSize * 2;
		 * outArr should be array of size complexSize * 2;
		 *
		 * inArr should be in the following format:
		 * [ x0.re, x1.re, ..., xn.re,
		 *   x0.im, x1.im, ..., xn.im ].
		 *
		 * Then after calling this function the outArr would be:
		 *	[x0.re, x0.im, x1.re, x1.im, ..., xn.re, xn.im].
		 */
		template<typename T>
		static void interleaveComplexArray(const T* inArr, T* outArr,
				int complexSize) {
			for (int i = 0; i < complexSize; i++) {
				outArr[i * 2] = inArr[i];
				outArr[i * 2 + 1] = inArr[complexSize + i];
			}
		}

	private:

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed,
				rev::vector1_type) {
			std::mt19937 engine(seed);

			typedef typename ArrayWrapper1D<T>::base_type base_type;

			std::function<T(T&)> func = [&](T & val) {
				val = generateNumber<base_type > (engine, min, max);
				return val;
			};

			std::transform(iterBegin, iterEnd, iterBegin, func);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, rev::vector1_type t) {

			fiilVec<T>(iterBegin, iterEnd, min, max, (int) time(0), t);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed,
				rev::vector2_type) {
			std::mt19937 engine(seed);

			typedef typename ArrayWrapper1D<T>::base_type base_type;

			std::function<T(T&)> func = [&](T & val) {
				val.x = generateNumber<base_type > (engine, min, max);
				val.y = generateNumber<base_type > (engine, min, max);
				return val;
			};

			std::transform(iterBegin, iterEnd, iterBegin, func);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, rev::vector2_type t) {
			fiilVec<T>(iterBegin, iterEnd, min, max, (int) time(0), t);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed,
				rev::vector3_type) {
			std::mt19937 engine(seed);

			typedef typename ArrayWrapper1D<T>::base_type base_type;

			std::function<T(T&)> func = [&](T & val) {
				val.x = generateNumber<base_type > (engine, min, max);
				val.y = generateNumber<base_type > (engine, min, max);
				val.z = generateNumber<base_type > (engine, min, max);
				return val;
			};

			std::transform(iterBegin, iterEnd, iterBegin, func);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, rev::vector3_type t) {
			fiilVec<T>(iterBegin, iterEnd, min, max, (int) time(0), t);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, int seed,
				rev::vector4_type) {
			std::mt19937 engine(seed);

			typedef typename ArrayWrapper1D<T>::base_type base_type;

			std::function<T(T&)> func = [&](T & val) {
				val.x = generateNumber<base_type > (engine, min, max);
				val.y = generateNumber<base_type > (engine, min, max);
				val.z = generateNumber<base_type > (engine, min, max);
				val.w = generateNumber<base_type > (engine, min, max);
				return val;
			};

			std::transform(iterBegin, iterEnd, iterBegin, func);
		}

		template<typename T>
		static void fiilVec(rev::ArrayIterator<T> iterBegin,
				rev::ArrayIterator<T> iterEnd,
				typename rev::type_traits<T>::base_type min,
				typename rev::type_traits<T>::base_type max, rev::vector4_type t) {
			fiilVec<T>(iterBegin, iterEnd, min, max, (int) time(0), t);
		}
	};
}

#endif /* REVARRAYUTIL_H_ */
