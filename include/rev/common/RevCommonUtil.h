/* 
 * File:   RevCommonUtil.h
 * Author: Revers
 *
 * Created on 11 sierpień 2012, 09:06
 */

#ifndef REVCOMMONUTIL_H
#define	REVCOMMONUTIL_H

#include <string>

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevStringUtil.h>

namespace rev {

    class REVCOMMON_API CommonUtil {
    public:
        static const std::string getDate();
		
		static std::string getSizeMB(int size) {
            float sz = (float) (size) / (1024.0f * 1024.f);
            return StringUtil::format("%.3f MB", sz);
        }
        
        static std::string getSizeKB(int size) {
            float sz = (float) (size) / 1024.0f;
            return StringUtil::format("%.3f KB", sz);
        }
    };
}

#endif	/* REVCOMMONUTIL_H */

