/*
 * RevArrayWrapper.h
 *
 *  Created on: 02-09-2012
 *      Author: Revers
 */

#ifndef REVARRAY_H_
#define REVARRAY_H_

#include <vector>
#include <memory>
#include <iostream>
#include <iterator>

#include <rev/common/RevCommonConfig.h>

#include <rev/common/RevAssert.h>
#include <rev/common/RevDataTypes.h>

#pragma GCC diagnostic ignored "-Wreorder"

namespace rev {

	template<typename T>
	class ArrayIterator: public std::iterator<std::forward_iterator_tag, T> {
		T* p;
	public:
		ArrayIterator(T* x) :
				p(x) {
		}
		ArrayIterator(const ArrayIterator& mit) :
				p(mit.p) {
		}
		ArrayIterator& operator++() {
			++p;
			return *this;
		}
		ArrayIterator operator++(int) {
			ArrayIterator tmp(*this);
			operator++();
			return tmp;
		}
		bool operator==(const ArrayIterator& rhs) {
			return p == rhs.p;
		}
		bool operator!=(const ArrayIterator& rhs) {
			return p != rhs.p;
		}
		T& operator*() {
			return *p;
		}
	};

	template<typename T>
	class ArrayWrapper1D {
	public:
		typedef T data_type;
		typedef typename rev::type_traits<T>::base_type base_type;
		const int component_size = sizeof(data_type);
	protected:
		int size = 0;
		T* ptr = nullptr;

		ArrayWrapper1D(int width) :
				size(width) {
		}
	public:

		ArrayWrapper1D() {
		}

		ArrayWrapper1D(T* ptr, int width) :
				ptr(ptr), size(width) {
		}

		virtual ~ArrayWrapper1D() {
		}

		T& operator()(int index) {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			assertMsg(index < size,
					"IndexOutOfBoundError: given: " << index << ", SIZE: " << size);

			return ptr[index];
		}

		ArrayIterator<T> begin() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ArrayIterator<T>(ptr);
		}

		ArrayIterator<T> end() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ArrayIterator<T>(ptr + size);
		}

		int getWidth() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return size;
		}

		int getSize() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return size;
		}

		virtual T * data() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ptr;
		}

		typename rev::type_traits<T>::vector_type getType() {
			typedef typename rev::type_traits<T>::vector_type VecType;
			return VecType::create();
		}
	};

	template<typename T>
	class ArrayWrapper2D {
	public:
		typedef T data_type;
		typedef typename rev::type_traits<T>::base_type base_type;
		const int component_size = sizeof(data_type);
	protected:
		int size = 0;
		int width = 0;
		int height = 0;
		T* ptr = nullptr;

		ArrayWrapper2D(int width, int height) :
				size(width * height), width(width), height(height) {
		}
	public:

		ArrayWrapper2D() {
		}

		ArrayWrapper2D(T* ptr, int width, int height) :
				size(width * height), width(width), height(height), ptr(ptr) {
		}

		virtual ~ArrayWrapper2D() {
		}

		T& operator()(int indexX, int indexY) {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			assertMsg(indexX < width && indexY < height,
					"IndexOutOfBoundError: given: (" << rev::a2s(indexX, indexY) << "), SIZE: (" << rev::a2s(width, height) << ")");
			// For an array of [ A ][ B ], we can index it by:
			// a * B + b
			// [y][x]
			// rows (index x) are contiguous in memory:
			return ptr[indexY * width + indexX];
		}

		ArrayIterator<T> begin() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ArrayIterator<T>(ptr);
		}

		ArrayIterator<T> end() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ArrayIterator<T>(ptr + size);
		}

		int getWidth() {
			assertMsg(ptr != nullptr && size != 0 && width != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size), R(width)));
			return width;
		}

		int getHeight() {
			assertMsg(ptr != nullptr && size != 0 && height != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size), R(height)));
			return height;
		}

		int getSize() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return size;
		}

		virtual T * data() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ptr;
		}

		typename rev::type_traits<T>::vector_type getType() {
			typedef typename rev::type_traits<T>::vector_type VecType;
			return VecType::create();
		}
	};

	template<typename T>
	class ArrayWrapper3D {
	public:
		typedef T data_type;
		typedef typename rev::type_traits<T>::base_type base_type;
		const int component_size = sizeof(data_type);
	protected:
		int size = 0;
		int width = 0;
		int height = 0;
		int depth = 0;
		T* ptr = nullptr;

		ArrayWrapper3D(int width, int height, int depth) :
				size(width * height * depth), width(width), height(height), depth(
						depth) {
		}
	public:

		ArrayWrapper3D() {
		}

		ArrayWrapper3D(T* ptr, int width, int height, int depth) :
				size(width * height * depth), width(width), height(height), depth(
						depth), ptr(ptr) {
		}

		virtual ~ArrayWrapper3D() {
		}

		T& operator()(int indexX, int indexY, int indexZ) {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			assertMsg(indexX < width && indexY < height && indexZ < depth,
					"IndexOutOfBoundError: given: (" << rev::a2s(indexX, indexY, indexZ) << "), SIZE: (" << rev::a2s(width, height, depth) << ")");
			// For an array of [ A ][ B ][ C ], we can index it by:
			// (a * B * C) + (b * C) + c
			// [z][y][x]
			// rows (index x) are contiguous in memory:
			return ptr[indexZ * height * width + indexY * width + indexX];
		}

		ArrayIterator<T> begin() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ArrayIterator<T>(ptr);
		}

		ArrayIterator<T> end() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ArrayIterator<T>(ptr + size);
		}

		int getWidth() {
			assertMsg(ptr != nullptr && size != 0 && width != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size), R(width)));
			return width;
		}

		int getHeight() {
			assertMsg(ptr != nullptr && size != 0 && height != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size), R(height)));
			return height;
		}

		int getDepth() {
			assertMsg(ptr != nullptr && size != 0 && depth != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size), R(depth)));
			return depth;
		}

		int getSize() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return size;
		}

		virtual T * data() {
			assertMsg(ptr != nullptr && size != 0,
					"ERROR: " << rev::a2s(R(ptr), R(size)));
			return ptr;
		}

		typename rev::type_traits<T>::vector_type getType() {
			typedef typename rev::type_traits<T>::vector_type VecType;
			return VecType::create();
		}
	};

	template<typename T>
	class Array1D: public ArrayWrapper1D<T> {
	public:
		Array1D(int width) :
				ArrayWrapper1D<T>(width) {
			ArrayWrapper1D<T>::ptr = new T[width];
		}

		~Array1D() {
			delete[] ArrayWrapper1D<T>::ptr;
		}
	};

	template<typename T>
	class Array2D: public ArrayWrapper2D<T> {
	public:
		Array2D(int width, int height) :
				ArrayWrapper2D<T>(width, height) {
			ArrayWrapper2D<T>::ptr = new T[width * height];
		}

		~Array2D() {
			delete[] ArrayWrapper2D<T>::ptr;
		}
	};

	template<typename T>
	class Array3D: public ArrayWrapper3D<T> {
	public:
		Array3D(int width, int height, int depth) :
				ArrayWrapper3D<T>(width, height, depth) {
			ArrayWrapper3D<T>::ptr = new T[width * height * depth];
		}

		~Array3D() {
			delete[] ArrayWrapper3D<T>::ptr;
		}
	};
}

template<typename V>
std::ostream& operator<<(std::ostream& out, rev::ArrayWrapper3D<V>& vec) {

	for (int k = 0; k < vec.getDepth(); k++) {
		out << ">>> ";
		for (int j = 0; j < vec.getHeight(); j++) {
			for (int i = 0; i < vec.getWidth(); i++) {
				out << vec(i, j, k) << ", ";
			}
			out << std::endl;
		}
	}

	return out;
}

template<typename V>
std::ostream& operator<<(std::ostream& out, rev::ArrayWrapper2D<V>& vec) {
	for (int j = 0; j < vec.getHeight(); j++) {
		for (int i = 0; i < vec.getWidth(); i++) {
			out << vec(i, j) << ", ";
		}
		out << std::endl;
	}

	return out;
}

template<typename V>
std::ostream& operator<<(std::ostream& out, rev::ArrayWrapper1D<V>& vec) {
	for (int i = 0; i < vec.getWidth(); i++) {
		out << vec(i) << ", ";
	}

	return out;
}

#endif /* REVARRAY_H_ */
