/* 
 * File:   IBuffer.hpp
 * Author: Revers
 *
 * Created on 26 czerwiec 2012, 20:07
 */

#ifndef REVIBUFFER_HPP
#define	REVIBUFFER_HPP

#include <cstddef>

namespace rev {

    template<typename T>
    class IBuffer {
    public:
        typedef T buffer_type;

        virtual
        bool create() = 0;

        virtual
        bool read(void* destPtr) = 0;

        virtual
        bool write(const void* sourcePtr) = 0;

        virtual
        int getSize() const = 0;

        virtual
        void destroy() = 0;

        virtual
        void swap(buffer_type& other) = 0;

        virtual
        ~IBuffer() {
        }

    protected:
        template<typename V>
        inline void swapHelper(V& a, V& b) {
            V temp = a;
            a = b;
            b = temp;
        }
    };
} // end namespace rev

#endif	/* REVIBUFFER_HPP */

