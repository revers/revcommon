/* 
 * File:   RevResourcePath.h
 * Author: Revers
 *
 * Created on 24 marzec 2013, 16:07
 */

#ifndef REVRESOURCEPATH_H
#define	REVRESOURCEPATH_H

#include <rev/common/RevCommonConfig.h>
#include <string>

#define REV_RESOURCES_PATH_KEY "resources.path"

namespace rev {

	class REVCOMMON_API ResourcePath {
		static bool inited;
		static std::string programPath;
		static std::string preferredPath;
		static std::string activePath;

		ResourcePath() = delete;
		~ResourcePath() = delete;

	public:
		/**
		 * IMPORTANT: Call this after rev::ConfigReader::load() (not before!).
		 */
		static void init(int argc, char** argv);

		static std::string get(const std::string& file, bool preferred = true);

		/**
		 * IMPORTANT: This function makes path relative only when
		 * the path is a sub-path of program executable's directory.
		 * Otherwise it returns the untouched argument.
		 *
		 * If the path is already relative (ie. isRelative(path) returns true)
		 * then it also returns the untouched argument.
		 */
		static std::string makeRelative(const std::string& path);

		static const std::string& getProgramPath() {
			return programPath;
		}

		static const std::string& getPreferredPath() {
			return preferredPath;
		}

		static const std::string& getActivePath() {
			return activePath;
		}

		/**
		 * Checks if the path is relative or not.
		 * IMPORTANT: Relative to any directory. This function do not
		 * check whether the path is a sub-path of the program executable's directory.
		 */
		static bool isRelative(const std::string& path);

	};
} // namespace rev

#endif	/* REVRESOURCEPATH_H */

