/*
 * RevArrayIterator.hpp
 *
 *  Created on: 06-12-2012
 *      Author: Revers
 */

#ifndef REVARRAYITERATOR_HPP_
#define REVARRAYITERATOR_HPP_

#include <iostream>
#include <iterator>

namespace rev {
    template<typename T>
    class ArrayIterator: public std::iterator<std::input_iterator_tag, T> {
        T* p;
        public:
        ArrayIterator(T* x) :
                p(x) {
        }
        ArrayIterator(const ArrayIterator& mit) :
                p(mit.p) {
        }
        ArrayIterator& operator++() {
            ++p;
            return *this;
        }
        ArrayIterator operator++(int) {
            ArrayIterator tmp(*this);
            operator++();
            return tmp;
        }
        bool operator==(const ArrayIterator& rhs) {
            return p == rhs.p;
        }
        bool operator!=(const ArrayIterator& rhs) {
            return p != rhs.p;
        }
        T& operator*() {
            return *p;
        }

        const T& operator*() const {
            return *p;
        }
    };
}

#endif /* REVARRAYITERATOR_HPP_ */
