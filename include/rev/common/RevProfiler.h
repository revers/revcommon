/* 
 * File:   RevProfiler.h
 * Author: Kamil
 *
 * Created on 29 sierpień 2012, 16:12
 */

#ifndef REVPROFILER_H
#define	REVPROFILER_H

#include <rev/common/RevCommonConfig.h>
#include <rev/common/RevTimer.h>

namespace rev {

    class REVCOMMON_API Profiler {
        double totalTime = 0.0;
        Timer timer;
    public:

        void start() {
            timer.start();
        }

        void stop() {
            timer.stop();
            totalTime += timer.getTimeInMicroSec();
        }

        void reset() {
            if (!timer.isStopped()) {
                timer.stop();
            }
            totalTime = 0.0;
        }

        double getTotalTimeMicro() const {
            return totalTime;
        }

        double getTotalTimeMs() const {
            return totalTime * 0.001;
        }

        double getTotalTimeSec() const {
            return totalTime * 0.000001;
        }

        double getLastTimeMicro() {
            return timer.getTimeInMicroSec();
        }

        double getLastTimeMs() {
            return timer.getTimeInMilliSec();
        }

        double getLastTimeSec() {
            return timer.getTimeInSec();
        }
    };
}

#endif	/* REVPROFILER_H */

