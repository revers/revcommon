/*
 * RevDelete.hpp
 *
 *  Created on: 25-11-2012
 *      Author: Revers
 */

#ifndef REVDELETE_HPP_
#define REVDELETE_HPP_

#define DEL(x) if(x) { delete x; x = nullptr; }
#define ARRDEL(x) if(x) { delete[] x; x = nullptr); }

#endif /* REVDELETE_HPP_ */
