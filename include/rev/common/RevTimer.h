/* 
 * File:   RevTimer.h
 * 
 * The author of orignal version of this class is: 
 * Song Ho Ahn (song.ahn (at) gmail.com)
 * (http://www.songho.ca/)
 * 
 * Created on 23 sierpień 2012, 14:26
 */

#ifndef REVTIMER_H
#define	REVTIMER_H

#include <rev/common/RevCommonConfig.h>

#ifdef REVCOMMON_WINDOWS
#include <windows.h>
#else          // Unix based system specific
#include <sys/time.h>
#endif

namespace rev {

    class REVCOMMON_API Timer {
    public:
        Timer();
        ~Timer();

        void start();
        void stop();
        double getTimeInSec();
        double getTimeInMilliSec();
        double getTimeInMicroSec();

        bool isStopped() {
            return stopped;
        }

    private:
        double startTimeInMicroSec; // starting time in micro-second
        double endTimeInMicroSec; // ending time in micro-second
        bool stopped; // stop flag 
#ifdef REVCOMMON_WINDOWS
        LARGE_INTEGER frequency; // ticks per second
        LARGE_INTEGER startCount; //
        LARGE_INTEGER endCount; //
#else
        timeval startCount; //
        timeval endCount;//
#endif
    };
}
#endif	/* REVTIMER_H */

