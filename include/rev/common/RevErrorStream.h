/* 
 * File:   RevErrorStream.h
 * Author: Kamil
 *
 * Created on 10 sierpień 2012, 18:28
 */

#ifndef REVERRORSTREAM_H
#define	REVERRORSTREAM_H

#include <fstream>
#include <iostream>
#include <functional>
#include <vector>

#include "RevCommonConfig.h"
#include "RevAssertMinimal.h"
#include "RevIErrorStreamListener.h"

#ifdef REVCOMMON_ERR_STREAM_TO_FILE
#include "RevFileErrorStreamListener.h"
#endif

#define REVCOMMON_ERR_STREAM_FILE __FILE__
#define REVCOMMON_ERR_STREAM_LINE __LINE__
#define REVCOMMON_ERR_STREAM_FUNCTION __PRETTY_FUNCTION__

#ifdef REVCOMMON_ERR_MSG_PRINT_TIME
#include "RevCommonUtil.h"
#define REV_ERROR_MSG(x) rev::errout << "[ERROR] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]: " << "[" << rev::CommonUtil::getDate() << "]:\n" << x << std::endl
#else
#define REV_ERROR_MSG(x) rev::errout << "[ERROR] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << x << std::endl
#endif

#define REV_WARN_MSG(x) rev::errout << "[WARN] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << x << std::endl

#define REV_INFO_MSG(x) rev::errout << "[INFO] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << x << std::endl

#ifndef NDEBUG
#define REV_DEBUG_MSG(x) rev::errout << "[DEBUG] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << x << std::endl

#define REV_DEBUG_WARN_MSG(x) rev::errout << "[WARN] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << x << std::endl

#define REV_TRACE_MSG(x) rev::errout << "[TRACE] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << x << std::endl

#define REV_TRACE_FUNCTION rev::errout << "[TRACE] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << __FUNCTION__ << std::endl
#define REV_TRACE_FUNCTION_IN rev::errout << "[TRACE] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << __FUNCTION__ << " IN" << std::endl
#define REV_TRACE_FUNCTION_OUT rev::errout << "[TRACE] " << rev::prv::errStreamPrettyFunc(REVCOMMON_ERR_STREAM_FUNCTION) \
    << "[line " << REVCOMMON_ERR_STREAM_LINE << "]:\n" << __FUNCTION__ << " OUT" << std::endl
#else
#define REV_DEBUG_MSG(x)
#define REV_DEBUG_WARN_MSG(x)
#define REV_TRACE_MSG(x)
#define REV_TRACE_FUNCTION
#define REV_TRACE_FUNCTION_IN
#define REV_TRACE_FUNCTION_OUT
#endif

#if defined(REVCOMMON_WINDOWS) && !defined(REVCOMMON_NO_COLORIZED_OUT)
#include <rev/common/win/RevConsoleColorizer.h>

#define REV_FG_RED rev::win::fg_red
#define REV_FG_GREEN rev::win::fg_green
#define REV_FG_BLUE rev::win::fg_blue
#define REV_FG_WHITE rev::win::fg_white
#define REV_FG_CYAN rev::win::fg_cyan
#define REV_FG_MAGENTA rev::win::fg_magenta
#define REV_FG_YELLOW rev::win::fg_yellow
#define REV_FG_BLACK rev::win::fg_black
#define REV_FG_GRAY rev::win::fg_gray
#define REV_BG_RED rev::win::bg_red
#define REV_BG_GREEN rev::win::bg_green
#define REV_BG_BLUE rev::win::bg_blue
#define REV_BG_WHITE rev::win::bg_white
#define REV_BG_CYAN rev::win::bg_cyan
#define REV_BG_MAGENTA rev::win::bg_magenta
#define REV_BG_YELLOW rev::win::bg_yellow
#define REV_BG_BLACK rev::win::bg_black
#define REV_BG_GRAY rev::win::bg_gray
#define REV_FG_LO_RED rev::win::fg_lo_red
#define REV_FG_LO_GREEN rev::win::fg_lo_green
#define REV_FG_LO_BLUE rev::win::fg_lo_blue
#define REV_FG_LO_WHITE rev::win::fg_lo_white
#define REV_FG_LO_CYAN rev::win::fg_lo_cyan
#define REV_FG_LO_MAGENTA rev::win::fg_lo_magenta
#define REV_FG_LO_YELLOW rev::win::fg_lo_yellow
#define REV_BG_LO_RED rev::win::bg_lo_red
#define REV_BG_LO_GREEN rev::win::bg_lo_green
#define REV_BG_LO_BLUE rev::win::bg_lo_blue
#define REV_BG_LO_WHITE rev::win::bg_lo_white
#define REV_BG_LO_CYAN rev::win::bg_lo_cyan
#define REV_BG_LO_MAGENTA rev::win::bg_lo_magenta
#define REV_BG_LO_YELLOW rev::win::bg_lo_yellow

#else

#define REV_FG_RED ""
#define REV_FG_GREEN ""
#define REV_FG_BLUE ""
#define REV_FG_WHITE ""
#define REV_FG_CYAN ""
#define REV_FG_MAGENTA ""
#define REV_FG_YELLOW ""
#define REV_FG_BLACK ""
#define REV_FG_GRAY ""
#define REV_BG_RED ""
#define REV_BG_GREEN ""
#define REV_BG_BLUE ""
#define REV_BG_WHITE ""
#define REV_BG_CYAN ""
#define REV_BG_MAGENTA ""
#define REV_BG_YELLOW ""
#define REV_BG_BLACK ""
#define REV_BG_GRAY ""
#define REV_FG_LO_RED ""
#define REV_FG_LO_GREEN ""
#define REV_FG_LO_BLUE ""
#define REV_FG_LO_WHITE ""
#define REV_FG_LO_CYAN ""
#define REV_FG_LO_MAGENTA ""
#define REV_FG_LO_YELLOW ""
#define REV_BG_LO_RED ""
#define REV_BG_LO_GREEN ""
#define REV_BG_LO_BLUE ""
#define REV_BG_LO_WHITE ""
#define REV_BG_LO_CYAN ""
#define REV_BG_LO_MAGENTA ""
#define REV_BG_LO_YELLOW ""

#endif

#define R_RED(X) REV_FG_RED << X
#define R_GREEN(X) REV_FG_GREEN << X
#define R_BLUE(X) REV_FG_BLUE << X
#define R_WHITE(X) REV_FG_WHITE << X
#define R_CYAN(X) REV_FG_CYAN << X
#define R_MAGENTA(X) REV_FG_MAGENTA << X
#define R_YELLOW(X) REV_FG_YELLOW << X
#define R_BLACK(X) REV_FG_BLACK << X
#define R_GRAY(X) REV_FG_GRAY << X
#define R_BG_RED(X) REV_BG_RED << X
#define R_BG_GREEN(X) REV_BG_GREEN << X
#define R_BG_BLUE(X) REV_BG_BLUE << X
#define R_BG_WHITE(X) REV_BG_WHITE << X
#define R_BG_CYAN(X) REV_BG_CYAN << X
#define R_BG_MAGENTA(X) REV_BG_MAGENTA << X
#define R_BG_YELLOW(X) REV_BG_YELLOW << X
#define R_BG_BLACK(X) REV_BG_BLACK << X
#define R_BG_GRAY(X) REV_BG_GRAY << X
#define R_FG_RED(X) REV_FG_LO_RED << X
#define R_FG_GREEN(X) REV_FG_LO_GREEN << X
#define R_FG_BLUE(X) REV_FG_LO_BLUE << X
#define R_FG_WHITE(X) REV_FG_LO_WHITE << X
#define R_FG_CYAN(X) REV_FG_LO_CYAN << X
#define R_FG_MAGENTA(X) REV_FG_LO_MAGENTA << X
#define R_FG_YELLOW(X) REV_FG_LO_YELLOW << X
#define R_BG_LO_RED(X) REV_BG_LO_RED << X
#define R_BG_LO_GREEN(X) REV_BG_LO_GREEN << X
#define R_BG_LO_BLUE(X) REV_BG_LO_BLUE << X
#define R_BG_LO_WHITE(X) REV_BG_LO_WHITE << X
#define R_BG_LO_CYAN(X) REV_BG_LO_CYAN << X
#define R_BG_LO_MAGENTA(X) REV_BG_LO_MAGENTA << X
#define R_BG_LO_YELLOW(X) REV_BG_LO_YELLOW << X

//------------------------------------------------
// For future wide string support (UTF-16):
#define rev_stdout std::cout 

namespace rev {

    namespace prv {
        inline const char* errStreamPrettyFunc(const char* c) {
            return c;
        }
    }

    //--------------------------------------------------------------------------
    // BasicErrorStreamBuffer
    //==========================================================================

    template<typename _CharT, typename _Traits>
    class REVCOMMON_API BasicErrorStreamBuffer: public std::basic_streambuf<_CharT,
            _Traits> {
    public:
        typedef std::vector<rev::IErrorStreamListener*> ListenerVector;
        typedef std::function<void(const char*)> ConstCharFunction;
        typedef std::vector<ConstCharFunction> FuncVector;

        typedef std::basic_streambuf<_CharT, _Traits> base_stream_type;
        typedef _CharT char_type;
        typedef _Traits traits_type;
        typedef typename traits_type::int_type int_type;
        typedef typename traits_type::pos_type pos_type;
        typedef typename traits_type::off_type off_type;
        private:
        bool useStdout;
        std::vector<char_type> lineBuffer;
        ListenerVector errStreamListeners;
        FuncVector errStreamCallbacks;
        public:

#ifdef REVCOMMON_ERR_STREAM_TO_FILE
        FileErrorStreamListener* fileErrListener;

        BasicErrorStreamBuffer() {
            lineBuffer.reserve(1024);
            fileErrListener = new FileErrorStreamListener(REVCOMMON_ERROR_FILE);
            addErrorStreamListener(fileErrListener);
            useStdout = true;
        }

        virtual ~BasicErrorStreamBuffer() {
            delete fileErrListener;
        }
#else

        BasicErrorStreamBuffer() {
            lineBuffer.reserve(1024);
            useStdout = true;
        }

        virtual ~BasicErrorStreamBuffer() {
        }
#endif

        /**
         * BasicErrorStreamBuffer is not the owner of a listener. You have to
         * remember to delete it on your own if it is a dynamically allocated object.
         * @param listener
         */
        void addErrorStreamListener(rev::IErrorStreamListener* listener) {
            errStreamListeners.push_back(listener);
        }

        ListenerVector& getErrorStreamListeners() {
            return errStreamListeners;
        }

        void addErrorStreamCallback(ConstCharFunction callbackFunc) {
            errStreamCallbacks.push_back(callbackFunc);
        }

        FuncVector& getFuncErrorStreamCallbacks() {
            return errStreamCallbacks;
        }

        bool isUseStdout() const {
            return useStdout;
        }

        void setUseStdout(bool useStdout) {
            this->useStdout = useStdout;
        }

    protected:

        virtual int_type overflow(int_type c) {
            char_type ct = static_cast<char_type>(c);

            if (useStdout) {
                rev_stdout<< ct;
            }

            if (c == '\n') {
                fflush(stdout);
                lineBuffer.push_back(0);

                const char* line = &lineBuffer[0];

                for (rev::IErrorStreamListener* listener : errStreamListeners) {
                    listener->print(line);
                }

                for (auto& callback : errStreamCallbacks) {
                    callback(line);
                }

                lineBuffer.clear();
            } else {
                lineBuffer.push_back(ct);
            }

            return c;
        }
    };

    //--------------------------------------------------------------------------
    // BasicErrorStream
    //==========================================================================

    template<typename _CharT, typename _Traits>
    class REVCOMMON_API BasicErrorStream: public std::basic_ostream<_CharT, _Traits> {
        BasicErrorStreamBuffer<_CharT, _Traits> errBuff;

    public:
        typedef std::vector<rev::IErrorStreamListener*> ListenerVector;
        typedef std::function<void(const char*)> ConstCharFunction;
        typedef std::vector<ConstCharFunction> FuncVector;

        BasicErrorStream() {
            std::basic_ostream<_CharT, _Traits>::init(&errBuff);
        }

        /**
         * BasicErrorStream is not the owner of a listener. You have to
         * remember to delete it on your own if it is a dynamically allocated object.
         * @param listener
         */
        void addErrorStreamListener(rev::IErrorStreamListener* listener) {
            errBuff.addErrorStreamListener(listener);
        }

        FuncVector& getErrorStreamListeners() {
            return errBuff.getErrorStreamListeners();
        }

        void addErrorStreamCallback(ConstCharFunction callbackFunc) {
            errBuff.addErrorStreamCallback(callbackFunc);
        }

        FuncVector& getFuncErrorStreamCallbacks() {
            return errBuff.getErrorStreamCallbacks();
        }

        bool isUseStdout() const {
            return errBuff.isUseStdout();
        }

        void setUseStdout(bool useStdout) {
            errBuff.setUseStdout(useStdout);
        }
    };

    typedef BasicErrorStream<char, std::char_traits<char> > ErrorStream;

    extern REVCOMMON_API ErrorStream errout;

    REVCOMMON_API bool removeErrorFile();
}

#endif	/* REVERRORSTREAM_H */

