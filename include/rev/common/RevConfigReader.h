/* 
 * File:   RevConfigReader.h
 * Author: Kamil
 *
 * Created on 5 wrzesień 2012, 09:49
 *
 * This is modified version of http://twit88.com/blog/2008/02/27/unix-a-c-configuration-reader/
 * 
 */

#ifndef REVCONFIGREADER_H
#define	REVCONFIGREADER_H

#include <string>
#include <iostream>
#include <map>

#include <rev/common/RevCommonConfig.h>

#define REV_CONFIG_FILE_NAME "config.properties"

namespace rev {

    class REVCOMMON_API ConfigReader : public std::map<std::string, std::string> {
    private:
        //static ConfigReader* _instance;
    public:

        ConfigReader() {
        }

        ~ConfigReader() {
        }

        static ConfigReader& getInstance();
        static ConfigReader& ref() {
            return getInstance();
        }

        static bool load(const std::string& initFileName = REV_CONFIG_FILE_NAME);

        float getFloat(const std::string& property, float defaultValue = 0.0f);

        double getDouble(const std::string& property, double defaultValue = 0.0);

        unsigned int getUint(const std::string& property, unsigned int defaultValue = 0);

        int getInt(const std::string& property, int defaultValue = 0);

        bool getBool(const std::string& property, bool defaultValue = false);

        std::string getString(const std::string& property,
                const char* defaultValue = "");
    };

}

#endif	/* REVCONFIGREADER_H */

