/* 
 * File:   RevIErrorStreamListener.h
 * Author: Kamil
 *
 * Created on 14 sierpień 2012, 14:13
 */

#ifndef REVIERRORSTREAMLISTENER_H
#define	REVIERRORSTREAMLISTENER_H

#include <rev/common/RevCommonConfig.h>

namespace rev {

    class REVCOMMON_API IErrorStreamListener {
    public:

        virtual ~IErrorStreamListener() {
        }

        /**
         * @param line - line of error stream (without newline sign '\n' at the end).
         */
        virtual void print(const char* line) = 0;
    };
}

#endif	/* REVIERRORSTREAMLISTENER_H */

