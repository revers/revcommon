/* 
 * File:   RevIOffsetBuffer.hpp
 * Author: Revers
 *
 * Created on 2 lipiec 2012, 19:06
 */

#ifndef REVIOFFSETBUFFER_HPP
#define	REVIOFFSETBUFFER_HPP

#include "RevIBuffer.hpp"

namespace rev {

    template<typename T>
    class IOffsetBuffer : public IBuffer<T> {
    public:
        /**
         * @Override
         */
        virtual
        bool create() = 0;

        /**
         * @Override
         */
        virtual
        bool read(void* destPtr) = 0;

        /**
         * @Override
         */
        virtual
        bool write(const void* sourcePtr) = 0;

        /**
         * @Override
         */
        virtual
        int getSize() const = 0;

        /**
         * @Override
         */
        virtual
        void destroy() = 0;
        
        /**
         * @Override
         */
        virtual 
        void swap(typename IBuffer<T>::buffer_type& other) = 0;

        virtual
        bool write(
                const void* sourcePtr,
                int offset,
                int bytes) = 0;

        virtual
        bool read(
                void* destPtr,
                int offset,
                int bytes) = 0;
    };
}

#endif	/* REVIOFFSETBUFFER_HPP */

