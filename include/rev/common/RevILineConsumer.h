/* 
 * File:   RevILineConsumer.h
 * Author: Revers
 *
 * Created on 13 sierpień 2012, 21:18
 */

#ifndef REVILINECONSUMER_H
#define	REVILINECONSUMER_H

#include <rev/common/RevCommonConfig.h>

namespace rev {

    class REVCOMMON_API ILineConsumer {
    public:

        virtual ~ILineConsumer() {
        }

        virtual void consume(const char* line) = 0;
    };
}


#endif	/* REVILINECONSUMER_H */

