/* 
 * File:   MathUtil.h
 * Author: Kamil
 *
 * Created on 11 wrzesień 2012, 13:28
 */

#ifndef REVMATHUTIL_H
#define	REVMATHUTIL_H

#include <rev/common/RevCommonConfig.h>

namespace rev {

    class REVCOMMON_API MathUtil {
    public:
        
        static inline bool isPowerOfTwo(int x) {
            return ((x > 0) && !(x & (x - 1)));
        }

        static inline bool isPowerOfTwo(unsigned int x) {
            return ((x != 0) && !(x & (x - 1)));
        }
    };
}

#endif	/* REVMATHUTIL_H */

