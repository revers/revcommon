/*
 * RevDataGenerator.h
 *
 *  Created on: 01-09-2012
 *      Author: Revers
 */

#ifndef REVDATAGENERATOR_H_
#define REVDATAGENERATOR_H_

#include <vector>
#include <algorithm>
#include <functional>
#include <memory>

#include <ctime>
#include <random>

#include <rev/common/RevDataTypes.h>
#include <rev/common/RevArray.h>

namespace rev {

	namespace prv {

		/**
		 * Int type
		 */
		template<typename T>
		T generateNumberImpl(std::mt19937& engine, const T& min, const T& max,
				std::false_type) {

			static_assert(std::is_integral<T>::value,
					"You are trying to use non integral number!!");

			std::uniform_int_distribution<T> dis(min, max);

			return dis(engine);
		}

		/**
		 * Float type
		 */
		template<typename T>
		T generateNumberImpl(std::mt19937& engine, const T& min, const T& max,
				std::true_type) {

			static_assert(std::is_floating_point<T>::value,
					"You are trying to use non floating point number!!");

			std::uniform_real_distribution<T> dis(min, max);

			return dis(engine);
		}
	}

	template<typename T>
	T generateNumber(std::mt19937& engine, const T& min, const T& max) {
		return prv::generateNumberImpl(engine, min, max,
				std::is_floating_point<T>());
	}

	template<typename T>
	T generateNumber(const T& min, const T& max, int seed) {
		std::mt19937 engine(seed);
		return generateNumber(engine, min, max);
	}

	template<typename T>
	T generateNumber(const T& min, const T& max) {
		std::mt19937 engine(time(0));
		return generateNumber(engine, min, max, time(0));
	}

} // end namespace rev
#endif /* REVDATAGENERATOR_H_ */
